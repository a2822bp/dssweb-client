#!/usr/bin/env python

PROJECT = 'dsscli'

VERSION = '1.0'

from setuptools import setup, find_packages

try:
    long_description = open('README.rst', 'rt').read()
except IOError:
    long_description = ''

setup(
    name=PROJECT,
    version=VERSION,

    description='DSSWeb Client',
    long_description=long_description,

    author='LRZ',

    url='https://github.lrz.de',
    download_url='https://github.lrz.de',

    classifiers=['Development Status :: 5 - Production/Stable',
                 'Programming Language :: Python',
                 'Programming Language :: Python 3'
                 ],

    platforms=['Any'],

    scripts=[],

    provides=[],
    install_requires=['cliff', 'coreapi>=2.3.1'],

    namespace_packages=[],
    packages=find_packages(),
    package_data={'': ['dfnca.pem']},
    include_package_data=True,
    # Define command mappings
    entry_points={
        'console_scripts': [
            'dsscli=dssclient.main:main'
        ],
        'dssclient': [
            'login=dssclient.auth:Login',
            'project_list=dssclient.projects:ListProjects',
            'project_show=dssclient.projects:ShowProject',
            'dss_pool_list=dssclient.pools:ListPools',
            'dss_pool_show=dssclient.pools:ShowPool',
            'dss_pool_show_id=dssclient.pools:ShowPoolID',
            'dss_container_list=dssclient.containers:ListDSSContainers',
            'dss_container_show=dssclient.containers:ShowDSSContainer',
            'dss_container_show_id=dssclient.containers:ShowDSSContainerID',
            'dss_container_create=dssclient.containers:DSSContainerCreate',
            'dss_container_update=dssclient.containers:UpdateDSSContainer',
            'dss_container_delete=dssclient.containers:DeleteDSSContainer',
            'dss_nfsexport_create=dssclient.exports:DSSNFSExportCreate',
            'dss_nfsexport_list=dssclient.exports:ListDSSNFSExports',
            'dss_nfsexport_show=dssclient.exports:ShowDSSNFSExport',
            'dss_nfsexport_update=dssclient.exports:UpdateDSSNFSExport',
            'dss_nfsexport_delete=dssclient.exports:DeleteDSSNFSExport',
            'dss_invitation_list=dssclient.invitations:ListDSSInvitation',
            'dss_invitation_create=dssclient.invitations:DSSInvitationCreate',
            'dss_invitation_show_id=dssclient.invitations:ShowDSSInvitationID',
            'dss_invitation_show=dssclient.invitations:ShowDSSInvitation',
            'dss_invitation_update=dssclient.invitations:UpdateDSSInvitation',
            'dss_invitation_delete=dssclient.invitations:DeleteDSSInvitation',
            'dss_autogrplnk_list=dssclient.autogrplnk:ListDSSAutoGrpLink',
            'dss_autogrplnk_show=dssclient.autogrplnk:ShowDSSAutoGrpLink',
            'dss_autogrplnk_create=dssclient.autogrplnk:CreateDSSAutoGrpLink',
            'dss_autogrplnk_update=dssclient.autogrplnk:UpdateDSSAutoGrpLink',
            'dss_autogrplnk_delete=dssclient.autogrplnk:DeleteDSSAutoGrpLink',
            #'dss_globus_invitation_list=dssclient.globusinvitations:ListDSSGlobusInvitation',
            #'dss_globus_invitation_create=dssclient.globusinvitations:CreateDSSGlobusInvitation',
            #'dss_globus_invitation_show=dssclient.globusinvitations:ShowDSSGlobusInvitationID',
            #'dss_globus_invitation_delete=dssclient.globusinvitations:DeleteDSSGlobusInvitation',
            'dss_storage_list=dssclient.dsspools:ListDSSPools',
            'dss_storage_show=dssclient.dsspools:ShowDSSPool',
            'dss_storage_show_id=dssclient.dsspools:ShowDSSPoolID',
            'dss_storage_grant_list=dssclient.projectquotas:ListProjectQuotas',
            'dss_storage_grant_show=dssclient.projectquotas:ShowProjectQuota',
            'dss_storage_grant_create=dssclient.projectquotas:CreateProjectQuota',
            'dss_storage_grant_update=dssclient.projectquotas:UpdateProjectQuota',
            'dss_storage_grant_delete=dssclient.projectquotas:DeleteProjectQuota',
            'dss_passwd_list=dssclient.invitations:GeneratePasswd',
            'dss_group_list=dssclient.containers:GenerateGroupFile',
            'datacurator_list=dssclient.datacurators:ListDataCurators',
            'datacurator_show=dssclient.datacurators:ShowDataCurator',
            'datacurator_create=dssclient.datacurators:CreateDataCurator',
            'datacurator_update=dssclient.datacurators:UpdateDataCurator',
            'datacurator_delete=dssclient.datacurators:DeleteDataCurator',
            'dss_config_list=dssclient.containerconfigrelations:ListContainerConfigRelation',
            'dss_config_show=dssclient.containerconfigrelations:ShowContainerConfigRelation',
            'dss_config_create=dssclient.containerconfigrelations:CreateContainerConfigRelation',
            'dss_config_delete=dssclient.containerconfigrelations:DeleteContainerConfigRelation',
        ],
    },

    zip_safe=False,
)
