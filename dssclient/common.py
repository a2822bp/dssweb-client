"""Define common helper functions and classes."""


def get_item(plist, value, key):
    """
    Scan a list of hashes for a particular key value pair.

    Returns the first list item that matches.
    """
    item = []
    try:
        item = [
            p for p in plist if p[key] == value
        ][0]
    except:
        raise RuntimeError(
            "Item {} via key {} could not be found".format(value, key)
        )
    return item


def get_items(plist, value, key, subkey=None):
    """
    Scan a list of hashes for a particular key value pair.

    Returns all list items that match.
    If subkey argument is specified, it scans in a list of hashes of hashes
    for the particular value (l[key][subkey] == value)
    """
    items = []
    try:
        if not subkey:
            items = [
                p for p in plist if p[key] == value
            ]
        else:
            items = [
                p for p in plist if p[key][subkey] == value
            ]

    except:
        if not subkey:
            raise RuntimeError(
                "Items {} via key {} could not be found".format(value, key)
            )
        else:
            raise RuntimeError(
                "Items {} via key {}, subkey {} could not be found".format(
                    value, key, subkey
                )
            )
    return items
