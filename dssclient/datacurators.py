import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.auth import Token
from dssclient.common import get_item
from dssclient.dssapi.datacurators import DataCurator
from dssclient.dssapi.projects import DSSWebProjects


class ListDataCurators(Lister):
    """List all available data curator grants."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add optional arguments poolname and projectname"""
        parser = super(ListDataCurators, self).get_parser(prog_name)
        parser.add_argument('--projectname', nargs='?', help='Filter by name of project')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota list command"""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        filter_fields = {}
        datacurators = DataCurator(token=token)
        if parsed_args.projectname:
            filter_fields['data_project__name'] = parsed_args.projectname

        dclist = datacurators.list_data_curator(**filter_fields)
        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()

        # Prepare the output
        headers = ('ID',
                   'Data Project',
                   'Username',
                   'Firstname',
                   'Lastname',
                   'Is primary',
                   'Created',
                   )

        payload = ((
            p['id'],
            get_item(prlist, p['data_project'], 'url')['name'],
            p['username'],
            p['firstname'],
            p['lastname'],
            p['is_primary'],
            p['created'],
        ) for p in dclist)

        return (headers, payload)


class ShowDataCurator(ShowOne):
    """Show details about a particular data curator grant."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments quota id."""
        parser = super(ShowDataCurator, self).get_parser(prog_name)
        parser.add_argument('curatorid', nargs=1, help='ID of the data curator to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota show command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        datacurators = DataCurator(token=token)

        dc = datacurators.list_data_curator(id=parsed_args.curatorid[0])
        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()
        if len(dc) != 1:
            raise RuntimeError(
                'Data Curaotr {} does not exist'.format(parsed_args.curatorid[0]))
        dc = dc[0]

        # Prepare the output
        rows = ('ID',
                'Data Project',
                'Username',
                'Firstname',
                'Lastname',
                'Is primary',
                'Created',)

        data = (dc['id'],
                get_item(prlist, dc['data_project'], 'url')['name'],
                dc['username'],
                dc['firstname'],
                dc['lastname'],
                dc['is_primary'],
                dc['created'])

        return (rows, data)


class UpdateDataCurator(ShowOne):
    """Update details about an existing data curator grant."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(UpdateDataCurator, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('curatorid', nargs=1, help='ID of the data curator to delete')
        mutual_excl_group = parser.add_mutually_exclusive_group(required=True)
        mutual_excl_group.add_argument('--primary', required=False, action='store_true',
                                       help='Use as primary data curator to whom all data of uninvited or deleted users will be transfered to')
        mutual_excl_group.add_argument('--notprimary', required=False, action='store_true',
                                       help='Remove primary attribute from this data curator')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota delete command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        body = {}
        body['id'] = parsed_args.curatorid[0]
        body['is_primary'] = parsed_args.primary

        datacurators = DataCurator(token=token)

        dc = datacurators.update_data_curator(**body)
        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()

        # Prepare the output
        rows = ('ID',
                'Data Project',
                'Username',
                'Firstname',
                'Lastname',
                'Is primary',
                'Created',)

        data = (dc['id'],
                get_item(prlist, dc['data_project'], 'url')['name'],
                dc['username'],
                dc['firstname'],
                dc['lastname'],
                dc['is_primary'],
                dc['created'])

        return (rows, data)


class CreateDataCurator(ShowOne):
    """Create and show details about a new data curator grant."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments poolname, projectname, quota gb, files, container."""
        parser = super(CreateDataCurator, self).get_parser(prog_name)

        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--projectname', required=True,
                                 help='Name of the project to assign the data curator to')
        input_group.add_argument('--username', required=True,
                                 help='Username of the person that shall become data curator for the project')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--primary', required=False, action='store_true',
                                 help='Use as primary data curator to whom all data of uninvited or deleted users will be transfered to')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota create command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        projects = DSSWebProjects(token=token)
        project = projects.list_projects(name=parsed_args.projectname)

        if len(project) != 1:
            raise RuntimeError(
                'Project {} does not exist'.format(parsed_args.projectname, parsed_args.poolname))
        project = project[0]

        body = {}
        body['data_project'] = project['url']
        body['username'] = parsed_args.username
        body['is_primary'] = parsed_args.primary

        datacurator = DataCurator(token=token)
        dc = datacurator.create_data_curator(**body)

        rows = ('ID',
                'Data Project',
                'Username',
                'Firstname',
                'Lastname',
                'Is primary',
                'Created',)

        data = (dc['id'],
                parsed_args.projectname,
                dc['username'],
                dc['firstname'],
                dc['lastname'],
                dc['is_primary'],
                dc['created'])

        return (rows, data)


class DeleteDataCurator(Command):
    """Delete a particular data curator grant."""
    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments quota id."""
        parser = super(DeleteDataCurator, self).get_parser(prog_name)

        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('curatorid', nargs=1, help='ID of the data curator to delete')

        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota delete command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        body = {}
        body['id'] = parsed_args.curatorid[0]

        datacurator = DataCurator(token=token)
        datacurator.delete_data_curator(**body)
        self.app.stdout.write('Successfully deleted data curator {}\n'.format(body['id']))
