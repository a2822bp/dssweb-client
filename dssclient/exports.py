import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.dssapi.exports import DSSWebDSSNFSExports
from dssclient.auth import Token
from dssclient.dssapi.containers import DSSWebDSSContainers

from dssclient.common import get_item


class ShowDSSNFSExport(ShowOne):
    """Show details about a particular DSS data container NFS export."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSNFSExport, self).get_parser(prog_name)
        parser.add_argument('exportid', nargs=1, help='ID of the NFS export to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        exp = DSSWebDSSNFSExports(token=token)
        export = exp.list_dss_nfs_exports(id=parsed_args.exportid[0])

        if len(export) != 1:
            raise RuntimeError(
                'Export {} does not exists'.format(parsed_args.exportid[0]))
        export = export[0]

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        # Prepare the output
        rows = (
            'ID', 'Container', 'IP', 'Access Mode', 'Status', 'Mount Path', 'Expires', 'Active Task', 'Created',
            'Last Update')

        data = (export['id'],
                get_item(clist, export['dssdatacontainer'], 'url')['name'],
                export['ip'],
                export['access_mode_name'],
                export['status_name'],
                export['export_path'],
                export['expires_at'],
                export['active_task_id'],
                export['created'],
                export['last_updated'],
                )
        return (rows, data)


class ListDSSNFSExports(Lister):
    """List all available DSS nfs exports."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """
        Add optional argument projectname.

        If projectname is specified, we only list pools for the particular
        project.
        """
        parser = super(ListDSSNFSExports, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?', help='Filter by name of the project')
        parser.add_argument('--poolname', nargs='?', help='Filter by name of the pool')
        parser.add_argument('--containername', nargs='?', help='Filter by name of the container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli container list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI

        exp = DSSWebDSSNFSExports(token=token)
        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        explist = []

        filter_fields = {}
        # Limit list to a parciular project, if specified by the user
        if parsed_args.projectname:
            filter_fields['dssdatacontainer__project_quota__data_project__name'] = parsed_args.projectname
        if parsed_args.poolname:
            filter_fields['dssdatacontainer__project_quota__data_pool__name'] = parsed_args.poolname
        if parsed_args.containername:
            filter_fields['dssdatacontainer__name'] = parsed_args.containername

        explist = exp.list_dss_nfs_exports(**filter_fields)

        # Prepare the output
        headers = ('ID', 'Container', 'IP', 'Access Mode', 'Status')
        payload = ((
            p['id'],
            get_item(clist, p['dssdatacontainer'], 'url')['name'],
            p['ip'],
            p['access_mode_name'],
            p['status_name'],
        ) for p in explist)
        return (headers, payload)


class DSSNFSExportCreate(ShowOne):
    """Create and show details about a new DSS data container NFS export."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        accmodes = ['READ_WRITE', 'READ_ONLY']

        parser = super(DSSNFSExportCreate, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--containername', required=True, help='Name of the container to export')
        input_group.add_argument('--ip', required=True, help='IP address to export the container to')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--accessmode', choices=accmodes, required=False, default='READ_WRITE',
                                 help='Access mode of the export')
        input_group.add_argument('--expires', required=False,
                                 help='Expire data at which the export will be automatically removed')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        container = containers.list_dss_containers(name=parsed_args.containername)

        if len(container) != 1:
            raise RuntimeError(
                'Container {} does not exist'.format(parsed_args.containername))
        container = container[0]
        body = {}
        body['dssdatacontainer'] = container['url']
        body['ip'] = parsed_args.ip
        if parsed_args.accessmode:
            body['access_mode'] = parsed_args.accessmode
        if parsed_args.expires:
            body['expires_at'] = parsed_args.expires
        export = DSSWebDSSNFSExports(token=token)
        exp = export.create_dss_nfs_export(**body)
        rows = (
            'ID', 'Container', 'IP', 'Access Mode', 'Status', 'Active Task', 'Expires At')
        data = (exp['id'],
                container['name'],
                exp['ip'],
                exp['access_mode_name'],
                exp['status_name'],
                exp['active_task_id'],
                exp['expires_at'],
                )

        return (rows, data)


class UpdateDSSNFSExport(ShowOne):
    """Update and show details about a DSS data container NFS export."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        accmodes = ['READ_WRITE', 'READ_ONLY']

        parser = super(UpdateDSSNFSExport, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        parser.add_argument('exportid', nargs=1, help='ID of the NFS export to update')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--accessmode', choices=accmodes, required=False, default='READ_WRITE',
                                 help='Access mode of the export')
        input_group.add_argument('--expires', required=False,
                                 help='Expire data at which the export will be automatically removed')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        export = DSSWebDSSNFSExports(token=token)
        exp = export.list_dss_nfs_exports(id=parsed_args.exportid[0])

        if len(exp) != 1:
            raise RuntimeError(
                'Export {} does not exists'.format(parsed_args.exportid[0]))

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()
        body = {}
        if parsed_args.accessmode:
            body['access_mode'] = parsed_args.accessmode
        if parsed_args.expires:
            body['expires_at'] = parsed_args.expires
        if body == {}:
            raise RuntimeError('No updated field specified')
        body['id'] = parsed_args.exportid[0]

        exp = export.update_dss_nfs_export(**body)
        rows = (
            'ID', 'Container', 'IP', 'Access Mode', 'Status', 'Active Task', 'Expires At')
        data = (exp['id'],
                get_item(clist, exp['dssdatacontainer'], 'url')['name'],
                exp['ip'],
                exp['access_mode_name'],
                exp['status_name'],
                exp['active_task_id'],
                exp['expires_at'],
                )

        return (rows, data)


class DeleteDSSNFSExport(Command):
    """Delete a particular DSS data container NFS export."""
    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        accmodes = ['READ_WRITE', 'READ_ONLY']

        parser = super(DeleteDSSNFSExport, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        parser.add_argument('exportid', nargs=1, help='ID of the NFS export to delete')
        return parser

    def take_action(self, parsed_args):
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()
        body = {}
        body['id'] = parsed_args.exportid[0]

        export = DSSWebDSSNFSExports(token=token)
        exp = export.delete_dss_nfs_export(**body)
        self.app.stdout.write('Successfully started deletion of export {}\n'.format(body['id']))
