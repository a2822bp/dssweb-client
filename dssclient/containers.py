"""Define the dsscli commands that operate on data containers."""

import logging
from time import sleep

import argparse
from cliff.lister import Lister
from cliff.show import ShowOne
from cliff.command import Command
from dssclient.dssapi.projects import DSSWebProjects

from dssclient.dssapi.pools import DSSWebPools

from dssclient.auth import Token
from dssclient.dssapi.containers import DSSWebDSSContainers

from dssclient.common import get_item


class ListDSSContainers(Lister):
    """List all available DSS data containers."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """
        Add optional argument projectname.

        If projectname is specified, we only list pools for the particular
        project.
        """
        parser = super(ListDSSContainers, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?', help='Filter by name of project')
        parser.add_argument('--poolname', nargs='?', help='Filter by name of pool')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli container list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI
        cont = DSSWebDSSContainers(token=token)
        contlist = []

        pools = DSSWebPools(token=token)
        polist = pools.list_pools()

        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()

        # Limit list to a parciular project, if specified by the user
        if parsed_args.projectname and parsed_args.poolname:
            contlist = cont.list_dss_containers(
                project_quota__data_project__name=parsed_args.projectname,
                project_quota__data_pool__name=parsed_args.poolname
            )
        elif parsed_args.projectname:
            contlist = cont.list_dss_containers(
                project_quota__data_project__name=parsed_args.projectname)
        elif parsed_args.poolname:
            contlist = cont.list_dss_containers(
                project_quota__data_pool__name=parsed_args.poolname)
        else:
            contlist = cont.list_dss_containers()

        # Prepare the output
        headers = ('ID', 'Project', 'Pool', 'Name', 'Status')
        payload = ((
            p['id'],
            get_item(prlist, get_item(polist, p['project_quota'], 'url')['data_project'], 'url')['name'],
            get_item(polist, p['project_quota'], 'url')['data_pool']['name'],
            p['name'],
            p['status_name']
        ) for p in contlist)
        return (headers, payload)


class ShowDSSContainer(ShowOne):
    """Show details about a particular DSS data container."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSContainer, self).get_parser(prog_name)
        parser.add_argument('containername', nargs=1, help='Name of the container to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            name=parsed_args.containername[0],
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername[0]))
        cont = cont[0]

        pools = DSSWebPools(token=token)
        polist = pools.list_pools()

        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()

        # Prepare the output
        rows = (
            'ID', 'Project', 'Pool', 'Name', 'Path', 'Description', 'Status', 'Active Task', 'Read/Write Group',
            'Read Only Group',
            'Quota GB', 'Used GB', 'Used GB Percentage', 'Quota Files', 'Used Files', 'Used Files Percentage',
            'Backup GB', 'Backup Files',
            'ID Streamline Mode', 'Data Protection Mode', 'Globus Sharing', 'Globus Endpoint ID',
            'Is Lightweight Container')

        data = (cont['id'],
                get_item(prlist, get_item(polist, cont['project_quota'], 'url')['data_project'], 'url')['name'],
                get_item(polist, cont['project_quota'], 'url')['data_pool']['name'],
                cont['name'],
                cont['leading_path'],
                cont['description'],
                cont['status_name'],
                cont['active_task_id'],
                cont['rw_group'],
                cont['ro_group'],
                cont['quota_gb'],
                cont['used_gb'],
                cont['used_gb_pct'],
                cont['quota_files'],
                cont['used_files'],
                cont['used_files_pct'],
                cont['backup_used_gb'],
                cont['backup_used_files'],
                cont['id_mode_name'],
                cont['protect_mode_name'],
                cont['globus_sharing_enabled'],
                cont['globus_share_endpoint_id'],
                cont['lightweight_container'],
                )
        if cont['migration_status'] != 7:
            rows = rows + ('Container Migration Status', 'Migration Source Pool', 'Migration Target Pool',
                           'Migration Source Path', 'Migration Target Path')
            data = data + (cont['migration_status_name'],
                           get_item(polist, cont['project_quota'], 'url')['data_pool']['name'],
                           get_item(polist, cont['next_project_quota'], 'url')['data_pool']['name'],
                           cont['path'],
                           cont['next_path'])
        return (rows, data)


class ShowDSSContainerID(ShowOne):
    """Show details about a particular DSS data container."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSContainerID, self).get_parser(prog_name)
        parser.add_argument('containerid', nargs=1, help='ID of the container to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            id=parsed_args.containerid[0],
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername[0]))
        cont = cont[0]

        pools = DSSWebPools(token=token)
        polist = pools.list_pools()

        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()

        # Prepare the output
        rows = (
            'ID', 'Project', 'Pool', 'Name', 'Path', 'Description', 'Status', 'Active Task', 'Read/Write Group',
            'Read Only Group',
            'Quota GB', 'Used GB', 'Used GB Percentage', 'Quota Files', 'Used Files', 'Used Files Percentage',
            'Backup GB', 'Backup Files',
            'ID Streamline Mode', 'Data Protection Mode', 'Globus Sharing', 'Globus Endpoint ID',
            'Is Lightweight Container')

        data = (cont['id'],
                get_item(prlist, get_item(polist, cont['project_quota'], 'url')['data_project'], 'url')['name'],
                get_item(polist, cont['project_quota'], 'url')['data_pool']['name'],
                cont['name'],
                cont['leading_path'],
                cont['description'],
                cont['status_name'],
                cont['active_task_id'],
                cont['rw_group'],
                cont['ro_group'],
                cont['quota_gb'],
                cont['used_gb'],
                cont['used_gb_pct'],
                cont['quota_files'],
                cont['used_files'],
                cont['used_files_pct'],
                cont['backup_used_gb'],
                cont['backup_used_files'],
                cont['id_mode_name'],
                cont['protect_mode_name'],
                cont['globus_sharing_enabled'],
                cont['globus_share_endpoint_id'],
                cont['lightweight_container'],
                )
        if cont['migration_status'] != 7:
            rows = rows + ('Container Migration Status', 'Migration Source Pool', 'Migration Target Pool',
                           'Migration Source Path', 'Migration Target Path')
            data = data + (cont['migration_status_name'],
                           get_item(polist, cont['project_quota'], 'url')['data_pool']['name'],
                           get_item(polist, cont['next_project_quota'], 'url')['data_pool']['name'],
                           cont['path'],
                           cont['next_path'])
        return (rows, data)


class DSSContainerCreate(ShowOne):
    """Create and show details about a new DSS data container."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        dpmodes = ['NONE', 'BACKUP_DAILY', 'BACKUP_WEEKLY', 'ARCHIVE_DAILY', 'ARCHIVE_WEEKLY']
        idmodes = ['NONE', 'NORMAL', 'STRICT', 'FORCE', 'WORK', 'PUBLIC_READ']
        gmodes = ['OFF', 'ON']

        parser = super(DSSContainerCreate, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--projectname', required=True, help='Name of the project to create the container for')
        input_group.add_argument('--poolname', required=True, help='Name of the DSS pool to create the container in')
        input_group.add_argument('--size', type=int, required=True,
                                 help='Maximum capacity in GB the container should be able to use')
        input_group.add_argument('--files', type=int, required=True,
                                 help='Maximum number of files the container should be able to use')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--description', required=False, help='A description for the container')
        input_group.add_argument('--dpmode', choices=dpmodes, required=False, default='NONE',
                                 help='Data protection policy to use for this container')
        input_group.add_argument('--idmode', choices=idmodes, required=False, default='NORMAL',
                                 help='ID Streamlining Mode to use for this container')
        input_group.add_argument('--globus-sharing', choices=gmodes, required=False, default='OFF',
                                 help='Enable sharing with external users via Globus Sharing')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        pools = DSSWebPools(token=token)
        pool = pools.list_pools(
            data_pool__name=parsed_args.poolname,
            data_project__name=parsed_args.projectname,
        )
        if len(pool) != 1:
            raise RuntimeError(
                'Pool {} does not exists for project {}'.format(parsed_args.poolname, parsed_args.projectname))
        pool = pool[0]

        body = {}
        body['project_quota'] = pool['url']
        body['quota_gb'] = parsed_args.size
        body['quota_files'] = parsed_args.files
        if parsed_args.description:
            body['description'] = parsed_args.description
        if parsed_args.dpmode:
            body['protect_mode'] = parsed_args.dpmode
        if parsed_args.idmode:
            body['id_mode'] = parsed_args.idmode
        if parsed_args.globus_sharing == 'ON':
            body['globus_sharing_enabled'] = True
        else:
            body['globus_sharing_enabled'] = False

        containers = DSSWebDSSContainers(token=token)
        cont = containers.create_dss_container(**body)
        rows = (
            'ID', 'Name', 'Description', 'Status', 'Active Task', 'Read/Write Group', 'Read Only Group',
            'Quota GB', 'Quota Files', 'ID Streamline Mode', 'Data Protection Mode', 'Globus Sharing',
            'Globus Endpoint ID')
        data = (cont['id'],
                cont['name'],
                cont['description'],
                cont['status_name'],
                cont['active_task_id'],
                cont['rw_group'],
                cont['ro_group'],
                cont['quota_gb'],
                cont['quota_files'],
                cont['id_mode_name'],
                cont['protect_mode_name'],
                cont['globus_sharing_enabled'],
                cont['globus_share_endpoint_id'],
                )

        return (rows, data)


class UpdateDSSContainer(ShowOne):
    """Update details about a particular DSS data container."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        dpmodes = ['NONE', 'BACKUP_DAILY', 'BACKUP_WEEKLY', 'ARCHIVE_DAILY', 'ARCHIVE_WEEKLY']
        parser = super(UpdateDSSContainer, self).get_parser(prog_name)
        gmodes = ['OFF', 'ON']
        s3modes = ['OFF', 'ON']
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        parser.add_argument('containername', nargs=1, help='Name of the container to update')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--size', type=int, required=False,
                                 help='Maximum capacity in GB the container should be able to use')
        input_group.add_argument('--files', type=int, required=False,
                                 help='Maximum number of files the container should be able to use')
        input_group.add_argument('--description', required=False, help='A description for the container')
        input_group.add_argument('--dpmode', choices=dpmodes, required=False,
                                 help='Data protection policy to use for this container')
        input_group.add_argument('--globus-sharing', choices=gmodes, required=False,
                                 help='Enable sharing with external users via Globus Sharing')
        input_group.add_argument('--migration-target-pool', required=False, help=argparse.SUPPRESS)
        input_group.add_argument('--projectname', required=False, help=argparse.SUPPRESS)
        input_group.add_argument('--migration-switch-over', required=False, action='store_true', help=argparse.SUPPRESS)
        input_group.add_argument('--migration-cleanup', required=False, action='store_true', help=argparse.SUPPRESS)
        input_group.add_argument('--s3', required=False, choices=s3modes, help=argparse.SUPPRESS)
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            name=parsed_args.containername[0],
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername[0]))

        cont = cont[0]
        body = {}
        if parsed_args.size:
            body['quota_gb'] = parsed_args.size
        if parsed_args.files:
            body['quota_files'] = parsed_args.files
        if parsed_args.description:
            body['description'] = parsed_args.description
        if parsed_args.globus_sharing == 'ON':
            body['globus_sharing_enabled'] = True
        elif parsed_args.globus_sharing == 'OFF':
            body['globus_sharing_enabled'] = False
        if parsed_args.dpmode:
            body['protect_mode'] = parsed_args.dpmode
        if parsed_args.migration_target_pool:
            pools = DSSWebPools(token=token)
            pool = pools.list_pools(
                data_pool__name=parsed_args.migration_target_pool,
                data_project__name=parsed_args.projectname,
            )
            if len(pool) != 1:
                raise RuntimeError(
                    'Pool {} does not exists for project {}'.format(parsed_args.poolname, parsed_args.projectname))
            body['next_project_quota'] = pool[0]['url']
        if parsed_args.migration_switch_over:
            body['migration_status'] = 3
        if parsed_args.migration_cleanup:
            body['migration_status'] = 6
        if parsed_args.s3 == 'ON':
            body['s3_enabled'] = True
        if parsed_args.s3 == 'OFF':
            body['s3_enabled'] = False

        if body == {}:
            raise RuntimeError('No updated field specified')
        body['id'] = cont['id']


        cont = containers.update_dss_container(**body)
        rows = (
            'ID', 'Name', 'Description', 'Status', 'Active Task', 'Read/Write Group', 'Read Only Group',
            'Quota GB', 'Quota Files', 'ID Streamline Mode', 'Data Protection Mode', 'Globus Sharing',
            'Globus Endpoint ID')
        data = (cont['id'],
                cont['name'],
                cont['description'],
                cont['status_name'],
                cont['active_task_id'],
                cont['rw_group'],
                cont['ro_group'],
                cont['quota_gb'],
                cont['quota_files'],
                cont['id_mode_name'],
                cont['protect_mode_name'],
                cont['globus_sharing_enabled'],
                cont['globus_share_endpoint_id'],
                )
        if cont['migration_status'] != 7:
            rows = rows + ('Container Migration Status',)
            data = data + (cont['migration_status_name'],)
        return (rows, data)


class GenerateGroupFile(Command):
    """Generate content for /etc/group file"""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """
        Add optional argument projectname.

        If projectname is specified, we only list pools for the particular
        project.
        """
        parser = super(GenerateGroupFile, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?', help='Filter by name of project')
        parser.add_argument('--containername', nargs='?', help='Filter by name of container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli container list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI
        cont = DSSWebDSSContainers(token=token)
        contlist = []

        filter_fields = {}
        # Limit list to a particular project, if specified by the user
        if parsed_args.projectname:
            filter_fields['project_quota__data_project__name'] = parsed_args.projectname
        if parsed_args.containername:
            filter_fields['name'] = parsed_args.containername

        contlist = cont.list_dss_containers(**filter_fields)
        self.app.stdout.write("dssusers:x:2222:\n")
        for container in contlist:
            self.app.stdout.write("{}:x:{}:\n".format(container['ro_group'], container['ro_gid']))
            self.app.stdout.write("{}:x:{}:\n".format(container['rw_group'], container['rw_gid']))


class DeleteDSSContainer(Command):
    """Delete a DSS Container."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeleteDSSContainer, self).get_parser(prog_name)
        parser.add_argument('containername', nargs=1, help='Name of the container to delete')
        parser.add_argument('--skipconfirmation', required=False, action='store_true')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            name=parsed_args.containername[0],
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername[0]))

        cont = cont[0]
        if not parsed_args.skipconfirmation:
            self.app.stdout.write('\n')
            self.app.stdout.write('\n')
            self.app.stdout.write('!!! ATTENTION POSSIBLE DANGEROUS OPERATION !!!\n')
            self.app.stdout.write('\n')
            self.app.stdout.write('You are about to remove data container {}.\n'.format(cont['name']))
            self.app.stdout.write(
                'This will DELETE ALL DATA in the container INCLUDING ALL BACKUP and ARCHIVE copies.\n')
            self.app.stdout.write('This operation is final and cannot be stopped or rolled back.\n')
            self.app.stdout.write('\n')
            self.app.stdout.write('\n')
            self.app.stdout.write('If you are really sure that you want to delete this container, please confirm\n')
            self.app.stdout.write('by typing in the container name:\n')
            self.app.stdout.write('\n')
            self.app.stdout.write('Confirm => ')
            self.app.stdout.flush()
            confirm = self.app.stdin.readline().rstrip()
            if confirm != cont['name']:
                self.app.stdout.write('Containernames do not match. Aborting deletion.\n')
            else:
                self.app.stdout.write('Containernames match. Initiating deletion of {} in:\n'.format(cont['name']))
                for foo in reversed(range(1, 11)):
                    self.app.stdout.write('{} ...\n'.format(foo))
                    sleep(1)
                self.app.stdout.write('You have been warned ...')
                containers.delete_data_container(id=cont['id'])
                self.app.stdout.write('Successfully initiated deletion of container {}!\n'.format(cont['name']))
        else:
            containers.delete_data_container(id=cont['id'])
            self.app.stdout.write('Successfully initiated deletion of container {}!\n'.format(cont['name']))
