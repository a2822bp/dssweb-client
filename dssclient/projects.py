"""Define the dsscli commands that operate on data projects."""
import logging
from cliff.lister import Lister
from cliff.show import ShowOne
from dssclient.auth import Token
from dssclient.dssapi.projects import DSSWebProjects


class ListProjects(Lister):
    """List all available data projects."""

    log = logging.getLogger(__name__)

    def take_action(self, parsed_args):
        """Define the dsscli project list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects from DSSWebAPI
        projects = DSSWebProjects(token=token)
        plist = projects.list_projects()

        # Prepare the output
        headers = ('ID', 'Name')
        payload = ((p['id'], p['name']) for p in plist)
        return (headers, payload)


class ShowProject(ShowOne):
    """Show details about a data project."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments projectname."""
        parser = super(ShowProject, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs=1, help='Name of the project to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli project show projectname command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects from DSSWebAPI
        projects = DSSWebProjects(token=token)
        # Get particular project from project list using filters
        project = projects.list_projects(name=parsed_args.projectname[0])
        if len(project) != 1:
            raise RuntimeError(
                'Project {} does not exist'.format(parsed_args.projectname[0]))
        project = project[0]
        # Prepare the output
        rows = ('ID', 'Name', 'Curator Group', 'Status', 'Description', 'Organisation', 'Department', 'Subdepartment', 'Project Start', 'Project End','URL')
        data = (project['id'],
                project['name'],
                project['curator_group'],
                project['status_name'],
                project['description'],
                project['organisation'],
                project['department'],
                project['subdepartment'],
                project['project_start'],
                project['project_end'],
                project['url'])
        return (rows, data)
