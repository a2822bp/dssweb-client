import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.dssapi.containerconfigrelations import DSSWebContainerConfigRelation
from dssclient.dssapi.containerconfigs import DSSWebContainerConfig
from dssclient.dssapi.containers import DSSWebDSSContainers
from dssclient.auth import Token

from dssclient.common import get_item


class ListContainerConfigRelation(Lister):
    """List either all available dss container configs or all applied configs"""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super().get_parser(prog_name)
        parser.add_argument('--applied', action='store_true',
                            help='List which container configs have been applied to which containers')
        parser.add_argument('--container', nargs='?', help='Filter by container name')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli config list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # init container config relation core api
        config_relation_api = DSSWebContainerConfigRelation(token=token)
        # init container config core api
        config_api = DSSWebContainerConfig(token=token)
        # init container core api
        container_api = DSSWebDSSContainers(token=token)

        if parsed_args.applied:
            filter_fields = {}
            if parsed_args.container:
                filter_fields['dssdatacontainer__name'] = parsed_args.container
            relation_list = config_relation_api.list_dss_container_config_relation(**filter_fields)
            config_list = config_api.list_dss_container_config()
            container_list = container_api.list_dss_containers()
            header = ('ID', 'Container', 'Config', 'Status', 'Applied Date')
            payload = ((
                r['id'],
                get_item(container_list, r['dssdatacontainer'], 'url')['name'],
                get_item(config_list, r['dsscontainerconfig'], 'url')['name'],
                r['status_name'],
                r['applied_at'],
            ) for r in relation_list)
        else:
            config_list = config_api.list_dss_container_config()
            header = ('ID', 'Name', 'Description')
            payload = ((
                c['id'],
                c['name'],
                c['description']
            ) for c in config_list)

        return (header, payload)


class CreateContainerConfigRelation(ShowOne):
    """Create and show details about a new DSS container config."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add arguments to create a new container config relation"""
        parser = super().get_parser(prog_name)
        container = parser.add_argument('--containername', required=True,
                                        help='Name of the container to apply the config to')
        config = parser.add_argument('--configname', required=True, help='Name of the config to apply')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli containerconfig relation create command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # init container core api
        container_api = DSSWebDSSContainers(token=token)
        # init config core api
        config_api = DSSWebContainerConfig(token=token)
        # init config relation core api
        config_relation_api = DSSWebContainerConfigRelation(token=token)

        container_list = container_api.list_dss_containers()
        config_list = config_api.list_dss_container_config()

        container = None
        try:
            container = get_item(container_list, parsed_args.containername, 'name')
        except RuntimeError:
            raise RuntimeError('Container {} does not exist'.format(parsed_args.containername))

        config = None
        try:
            config = get_item(config_list, parsed_args.configname, 'name')
        except RuntimeError:
            raise RuntimeError('Container {} does not exist'.format(parsed_args.containername))

        payload = {
            'dssdatacontainer': container['url'],
            'dsscontainerconfig': config['url']
        }

        # create config relation
        r = config_relation_api.create_dss_container_config_relation(**payload)

        header = ('ID', 'Container', 'Config', 'Status')
        payload = (
            r['id'],
            container['name'],
            config['name'],
            r['status_name']
        )

        return (header, payload)


class ShowContainerConfigRelation(ShowOne):
    """Show a container config relation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super().get_parser(prog_name)
        parser.add_argument('--configid', required=True, type=int, help='ID of the applied container config to show')
        return parser

    def take_action(self, parsed_args):
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # init containerconfigrelation api
        relation_api = DSSWebContainerConfigRelation(token=token)
        # init container core api
        container_api = DSSWebDSSContainers(token=token)
        # init config core api
        config_api = DSSWebContainerConfig(token=token)

        relation = relation_api.show_dss_container_config_relation(id=parsed_args.configid)
        container = container_api.read_dss_containers(id=relation['dssdatacontainer_id'])
        config = config_api.read_dss_container_config(id=relation['dsscontainerconfig_id'])

        header = ('ID', 'Container', 'Config', 'Status')
        payload = (
            relation['id'],
            container['name'],
            config['name'],
            relation['status_name']
        )

        return (header, payload)


class DeleteContainerConfigRelation(Command):
    """Delete a particular DSS container config relation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super().get_parser(prog_name)
        parser.add_argument('--configid', required=True, type=int, help='ID of the applied container config to revoke')
        return parser

    def take_action(self, parsed_args):
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        relation_id = parsed_args.configid

        config_relation_api = DSSWebContainerConfigRelation(token=token)

        config_relation_api.delete_dss_container_config_relation(id=relation_id)
        self.app.stdout.write('Successfully delete config {}.\n'.format(relation_id))
