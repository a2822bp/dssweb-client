"""Define the dsscli commands that operate on dsspools."""

import logging
from cliff.lister import Lister
from cliff.show import ShowOne
from dssclient.auth import Token
from dssclient.dssapi.dsspools import DSSWebDSSPools
from dssclient.common import get_item


class ListDSSPools(Lister):
    """List all available dsspools."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ListDSSPools, self).get_parser(prog_name)
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dss storage list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()
        # Get all dsspools from DSSWebAPI
        pools = DSSWebDSSPools(token=token)
        polist = pools.list_pools()

        # Prepare the output
        headers = ('ID', 'Name', 'Owner', 'Is Lightweight')
        payload = ((
            p['id'],
            p['name'],
            p['owner'],
            p['lightweight_pool']
        ) for p in polist)
        return (headers, payload)


class ShowDSSPool(ShowOne):
    """Show details about a particular dsspool."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments poolname."""
        parser = super(ShowDSSPool, self).get_parser(prog_name)
        parser.add_argument('poolname', nargs=1, help='Name of the pool to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dss storage show poolname command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        pools = DSSWebDSSPools(token=token)
        pool = pools.list_pools(
            name=parsed_args.poolname[0]
        )
        if len(pool) != 1:
            raise RuntimeError(
                'Pool {} does not exist'.format(parsed_args.poolname[0]))
        pool = pool[0]

        # Prepare the output
        rows = ('ID',
                'Name',
                'Description',
                'Hints',
                'Owner',
                'Total GB',
                'Quota Used GB',
                'Container Assgined GB',
                'Physically Used GB',
                'Total Files',
                'Quota Used Files',
                'Container Assinged Files',
                'Physically Used Files',
                'Total Containers',
                'Quota Used Containers',
                'Assigned Containers',
                'Is Lightweight Pool'
                )
        data = (pool['id'],
                pool['name'],
                pool['description'],
                pool['hints'],
                pool['owner'],
                pool['size_gb'],
                pool['used_gb'],
                pool['assigned_gb'],
                pool['phys_used_gb'],
                pool['num_files'],
                pool['used_files'],
                pool['assigned_files'],
                pool['phys_used_files'],
                pool['num_fs'],
                pool['used_fs'],
                pool['assigned_fs'],
                pool['lightweight_pool'],
                )

        return (rows, data)


class ShowDSSPoolID(ShowOne):
    """Show details about a particular dsspool."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments poolid."""
        parser = super(ShowDSSPoolID, self).get_parser(prog_name)
        parser.add_argument('poolid', nargs=1, help='ID of the pool to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dss storage show poolid command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        pools = DSSWebDSSPools(token=token)

        try:
            pool = pools.read_pools(
                id=parsed_args.poolid[0],
            )
        except Exception as e:
            raise RuntimeError(
                'Pool {} does not exist'.format(parsed_args.poolid[0]))

        # Prepare the output
        rows = ('ID',
                'Name',
                'Description',
                'Hints',
                'Owner',
                'Total GB',
                'Free GB',
                'Used GB',
                'Used GB Percentage',
                'Total Files',
                'Free Files',
                'Used Files',
                'Used Files Percentage',
                'Total Containers',
                'Free Containers',
                'Used Containers',
                'Used Containers Percentage',
                'Is Lightweight Pool')
        data = (pool['id'],
                pool['name'],
                pool['description'],
                pool['hints'],
                pool['owner'],
                pool['size_gb'],
                pool['free_gb'],
                pool['used_gb'],
                pool['used_gb_pct'],
                pool['num_files'],
                pool['free_files'],
                pool['used_files'],
                pool['used_files_pct'],
                pool['num_fs'],
                pool['free_fs'],
                pool['used_fs'],
                pool['used_cont_pct'],
                pool['lightweight_pool'],)
        return (rows, data)
