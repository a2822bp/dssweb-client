"""Define DSSWebAPI Data Pool functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebPools(DSSWebAPI):
    """Define the GET /datapools/ call in order to get all data pools."""

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['datapools', 'list']
        read_action = ['datapools', 'read']

    def list_pools(self, **kwargs):
        """Return a list of datapools (list of hashes)."""
        return self.list(**kwargs)

    def read_pools(self, **kwargs):
        """Return a list of datapools (list of hashes)."""
        return self.read(**kwargs)
