"""Define DSSWebAPI Data Export functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebDSSGlobusInvitations(DSSWebAPI):
    """Define the GET /dssglobusinvitations/ call in order to get all data pools."""

    acc_mode_trans = {'READ_WRITE': 'rw', 'READ_ONLY': 'r'}

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dssglobusinvitations', 'list']
        read_action = ['dssglobusinvitations', 'read']
        create_action = ['dssglobusinvitations', 'create']
        delete_action = ['dssglobusinvitations', 'delete']
        #partial_update_action = ['dssglobusinvitations', 'partial_update']

    def list_globus_invitations(self, **kwargs):
        """Return a list of dss nfs exports (list of hashes)."""
        return self.list(**kwargs)

    def read_globus_invitation(self, **kwargs):
        """Return a single dss nfs export (list of hashes)."""
        return self.read(**kwargs)

    def create_globus_invitation(self, **kwargs):
        """Create a new dss nfs export."""
        if kwargs['access_type']:
            kwargs['access_type'] = self.acc_mode_trans[kwargs['access_type']]
        return self.create(**kwargs)

    """def update_dss_invitation(self, **kwargs):
        if kwargs['access_type']:
            kwargs['access_type'] = self.acc_mode_trans[kwargs['access_type']]
        return self.partial_update(**kwargs)"""

    def delete_globus_invitation(self, **kwargs):
        return self.delete(**kwargs)