"""Define DSSWebAPI Data Pool functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebProjects(DSSWebAPI):
    """Define the GET /dataprojects/ call in order to get all data pools."""

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dataprojects', 'list']

    def list_projects(self, **kwargs):
        """Return a list of dataprojects (list of hashes)."""
        return self.list(**kwargs)
