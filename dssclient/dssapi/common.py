"""Define common helper and base functions and classes for DSSWebAPI."""
import coreapi
import os
import requests


class DSSWebConfig(object):
    """Class to encapsulate basic settings for the DSSWebAPI module."""

    base_endpoint = 'https://dssweb.dss.lrz.de:443/api/'
    version = 'v1'
    schema_endpoint = base_endpoint + 'schema/'
    base_path = ['api']


class DSSWebAPI(object):
    """
    Base class that defines an interface to coreapi.

    Basically this class defines a common constructor and common functions
    to interact with the coreapi in order to DRY.
    """

    def __init__(self, token=None):
        """Setup coreapi client."""
        self._endpoint = DSSWebConfig.base_endpoint
        self._init_meta_defaults()

        if self.Meta.auth:
            self._token = token
            auth = coreapi.auth.TokenAuthentication(
                scheme='JWT',
                token=self._token
                )
            session = requests.Session()
            session.verify = os.path.dirname(os.path.dirname(__file__)) + '/dfnca.pem'
            transport = coreapi.transports.HTTPTransport(auth=auth, session=session)
            self._api = coreapi.Client(transports=[transport])
        else:
            session = requests.Session()
            session.verify = os.path.dirname(os.path.dirname(__file__)) + '/dfnca.pem'
            transport = coreapi.transports.HTTPTransport(session=session)
            self._api = coreapi.Client(transports=[transport])
        self._schema = self._api.get(DSSWebConfig.schema_endpoint)
        self.default_params = {'version': DSSWebConfig.version}
        self._base = DSSWebConfig.base_path

    def list(self, **kwargs):
        """
        Generic list function for an API endpoint.

        Takes a numer of parameters as hash (key:value)
        Returns a list of hashes.
        """
        # Merge the default params with the params specified via **kwargs
        # If same parameter exists in both dicts, defaults get overwritten
        params = self.default_params.copy()
        params.update(**kwargs)
        return self._api.action(self._schema, self._base + self.Meta.list_action, params)

    def read(self, **kwargs):
        """
        Generic read function for an API endpoint.

        Takes a numer of parameters as hash (key:value)
        Returns a list of hashes.
        """
        # Merge the default params with the params specified via **kwargs
        # If same parameter exists in both dicts, defaults get overwritten
        params = self.default_params.copy()
        params.update(**kwargs)
        return self._api.action(self._schema, self._base + self.Meta.read_action, params)

    def create(self, **kwargs):
        """
        Generic create function for an API endpoint.

        Takes a numer of parameters as hash (key:value)
        Returns the API response as hash.
        """
        params = self.default_params.copy()
        params.update(**kwargs)
        return self._api.action(self._schema, self._base + self.Meta.create_action, params)

    def partial_update(self, **kwargs):
        """
        Generic partial update function for an API endpoint.

        Takes a numer of parameters as hash (key:value)
        Returns the API response as hash.
        """
        params = self.default_params.copy()
        params.update(**kwargs)
        return self._api.action(self._schema, self._base + self.Meta.partial_update_action, params)

    def delete(self, **kwargs):
        """
        Generic delete function for an API endpoint.

        Takes a numer of parameters as hash (key:value)
        Returns the API response as hash.
        """
        params = self.default_params.copy()
        params.update(**kwargs)
        return self._api.action(self._schema, self._base + self.Meta.delete_action, params)

    def _init_meta_defaults(self):
        if not hasattr(self.Meta, 'auth'):
            self.Meta.auth = True
