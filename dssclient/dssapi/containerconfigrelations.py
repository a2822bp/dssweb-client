from dssclient.dssapi.common import DSSWebAPI


class DSSWebContainerConfigRelation(DSSWebAPI):

    class Meta:
        list_action = ['dsscontainerconfig-relations', 'list']
        create_action = ['dsscontainerconfig-relations', 'create']
        delete_action = ['dsscontainerconfig-relations', 'delete']
        read_action = ['dsscontainerconfig-relations', 'read']

    def list_dss_container_config_relation(self, **kwargs):
        return self.list(**kwargs)

    def create_dss_container_config_relation(self, **kwargs):
        return self.create(**kwargs)

    def show_dss_container_config_relation(self, **kwargs):
        return self.read(**kwargs)

    def delete_dss_container_config_relation(self, **kwargs):
        return self.delete(**kwargs)
