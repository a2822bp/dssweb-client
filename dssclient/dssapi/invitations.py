"""Define DSSWebAPI Data Export functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebDSSInvitations(DSSWebAPI):
    """Define the GET /dssnfsexports/ call in order to get all data pools."""

    acc_mode_trans = {'READ_WRITE': 1, 'READ_ONLY': 2}
    mgr_mode_trans = {'YES': True, 'NO': False}

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dssinvitations', 'list']
        read_action = ['dssinvitations', 'read']
        create_action = ['dssinvitations', 'create']
        delete_action = ['dssinvitations', 'delete']
        partial_update_action = ['dssinvitations', 'partial_update']

    def list_dss_invitations(self, **kwargs):
        """Return a list of dss nfs exports (list of hashes)."""
        return self.list(**kwargs)

    def read_dss_invitation(self, **kwargs):
        """Return a single dss nfs export (list of hashes)."""
        return self.read(**kwargs)

    def create_dss_invitation(self, **kwargs):
        """Create a new dss nfs export."""
        if kwargs['access_type']:
            kwargs['access_type'] = self.acc_mode_trans[kwargs['access_type']]
        if kwargs['is_container_manager']:
            kwargs['is_container_manager'] = self.mgr_mode_trans[kwargs['is_container_manager']]
        return self.create(**kwargs)

    def update_dss_invitation(self, **kwargs):
        if 'access_type' in kwargs and kwargs['access_type']:
            kwargs['access_type'] = self.acc_mode_trans[kwargs['access_type']]
        if 'is_container_manager' in kwargs and kwargs['is_container_manager']:
            kwargs['is_container_manager'] = self.mgr_mode_trans[kwargs['is_container_manager']]
        return self.partial_update(**kwargs)

    def delete_dss_invitation(self, **kwargs):
        return self.delete(**kwargs)