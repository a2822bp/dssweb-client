"""Define DSSWebAPI Data Export functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebAutoGroupLink(DSSWebAPI):
    """Define the GET /dssautoinvitegrouplinks/ call in order to get all auto group links."""

    acc_mode_trans = {'READ_WRITE': 1, 'READ_ONLY': 2}

    origin_trans = {'DSS': 1, 'LinuxCluster': 2, 'SuperMUC': 3, 'TUMonline': 4, 'LMU-VZ': 5}


    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dssautoinvitegrouplinks', 'list']
        read_action = ['dssautoinvitegrouplinks', 'read']
        create_action = ['dssautoinvitegrouplinks', 'create']
        delete_action = ['dssautoinvitegrouplinks', 'delete']
        partial_update_action = ['dssautoinvitegrouplinks', 'partial_update']

    def list_dss_autogrplnk(self, **kwargs):
        """Return a list of dss nfs exports (list of hashes)."""
        return self.list(**kwargs)

    def read_dss_autogrplnk(self, **kwargs):
        """Return a single dss nfs export (list of hashes)."""
        return self.read(**kwargs)

    def create_dss_autogrplnk(self, **kwargs):
        """Create a new dss nfs export."""
        if kwargs['access_type']:
            kwargs['access_type'] = self.acc_mode_trans[kwargs['access_type']]
        if kwargs['sim_service']:
            kwargs['sim_service'] = self.origin_trans[kwargs['sim_service']]
        return self.create(**kwargs)

    def update_dss_autogrplnk(self, **kwargs):
        if kwargs['access_type']:
            kwargs['access_type'] = self.acc_mode_trans[kwargs['access_type']]
        return self.partial_update(**kwargs)

    def delete_dss_autogrplnk(self, **kwargs):
        return self.delete(**kwargs)