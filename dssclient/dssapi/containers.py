"""Define DSSWebAPI Data Container functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebDSSContainers(DSSWebAPI):
    """Define the GET /dsscontainers/ call in order to get all data pools."""

    id_mode_trans = {'NONE': 0, 'NORMAL': 1, 'STRICT': 2, 'FORCE': 3, 'WORK': 4, 'PUBLIC_READ': 5}
    protect_mode_trans = {'NONE': 0, 'BACKUP_DAILY': 1, 'BACKUP_WEEKLY': 2, 'ARCHIVE_DAILY': 3, 'ARCHIVE_WEEKLY': 4}
    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dssdatacontainers', 'list']
        read_action = ['dssdatacontainers', 'read']
        create_action = ['dssdatacontainers', 'create']
        partial_update_action = ['dssdatacontainers', 'partial_update']
        delete_action = ['dssdatacontainers', 'delete']

    def list_dss_containers(self, **kwargs):
        """Return a list of dsscontainers (list of hashes)."""
        return self.list(**kwargs)

    def read_dss_containers(self, **kwargs):
        """Return a list of dsscontainers (list of hashes)."""
        return self.read(**kwargs)

    def create_dss_container(self, **kwargs):
        """Create a new dsscontainer."""
        if kwargs['id_mode']:
            kwargs['id_mode'] = self.id_mode_trans[kwargs['id_mode']]
        if kwargs['protect_mode']:
            kwargs['protect_mode'] = self.protect_mode_trans[kwargs['protect_mode']]
        return self.create(**kwargs)

    def update_dss_container(self, **kwargs):
        if 'protect_mode' in kwargs and kwargs['protect_mode']:
            kwargs['protect_mode'] = self.protect_mode_trans[kwargs['protect_mode']]
        return self.partial_update(**kwargs)

    def delete_data_container(self, **kwargs):
        """Delete a single dsscontainer."""
        return self.delete(**kwargs)