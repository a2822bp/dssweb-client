"""Define DSSWebAPI Data Export functions."""
from dssclient.dssapi.common import DSSWebAPI


class DataCurator(DSSWebAPI):
    """Define the GET /projectquotas/ call in order to get all quotas."""

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['datacurators', 'list']
        read_action = ['datacurators', 'read']
        create_action = ['datacurators', 'create']
        delete_action = ['datacurators', 'delete']
        partial_update_action = ['datacurators', 'partial_update']

    def list_data_curator(self, **kwargs):
        """Return a list of data curators."""
        return self.list(**kwargs)

    def read_data_curator(self, **kwargs):
        """Return a single data curators."""
        return self.read(**kwargs)

    def create_data_curator(self, **kwargs):
        """Create a new data curators."""
        return self.create(**kwargs)

    def delete_data_curator(self, **kwargs):
        """Delete a single data curators."""
        return self.delete(**kwargs)

    def update_data_curator(self, **kwargs):
        """Update a data curator."""
        return self.partial_update(**kwargs)