"""Define DSSWebAPI Authentication functions."""

from dssclient.dssapi.common import DSSWebAPI


class DSSWebTokenAuth(DSSWebAPI):
    """Define the POST /api-token-auth/ call in order to get an auth token."""

    class Meta:
        """Configure DSSWebAPI base class."""

        auth = False
        create_action = ['api-token-auth', 'create']

    def get_token(self, username, password):
        """Authenticate to DSSWeb using user and pass and obtain JWT."""
        return self.create(
            username=username,
            password=password
        )


class DSSWebTokenRefresh(DSSWebAPI):
    """Define the POST /api-token-refresh/ call to get a new token."""

    class Meta:
        """Configure DSSWebAPI base class."""

        create_action = ['api-token-refresh', 'create']
        auth = False

    def refresh_token(self, token):
        """Authenticate to DSSWweb using old JWT and obtain new JWT."""
        return self.create(
            refresh=token
        )


class DSSWebTokenVerify(DSSWebAPI):
    """Define the POST /api-token-verify/ call to check an existing token."""

    class Meta:
        """Configure DSSWebAPI base class."""

        create_action = ['api-token-verify', 'create']
        auth = False

    def verify_token(self, token):
        """
        Authenticate to DSSWeb using current JWT and check if token is valid.

        Returns True if token is valid, False otherwhise.
        """
        try:
            self.create(token=token)
        except:
            return False
        return True
