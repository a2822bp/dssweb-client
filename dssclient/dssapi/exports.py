"""Define DSSWebAPI Data Export functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebDSSNFSExports(DSSWebAPI):
    """Define the GET /dssnfsexports/ call in order to get all data pools."""

    acc_mode_trans = {'READ_WRITE': 1, 'READ_ONLY': 2}

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dssnfsexports', 'list']
        read_action = ['dssnfsexports', 'read']
        create_action = ['dssnfsexports', 'create']
        delete_action = ['dssnfsexports', 'delete']
        partial_update_action = ['dssnfsexports', 'partial_update']

    def list_dss_nfs_exports(self, **kwargs):
        """Return a list of dss nfs exports (list of hashes)."""
        return self.list(**kwargs)

    def read_dss_nfs_export(self, **kwargs):
        """Return a single dss nfs export (list of hashes)."""
        return self.read(**kwargs)

    def create_dss_nfs_export(self, **kwargs):
        """Create a new dss nfs export."""
        if kwargs['access_mode']:
            kwargs['access_mode'] = self.acc_mode_trans[kwargs['access_mode']]
        return self.create(**kwargs)

    def update_dss_nfs_export(self, **kwargs):
        if kwargs['access_mode']:
            kwargs['access_mode'] = self.acc_mode_trans[kwargs['access_mode']]
        return self.partial_update(**kwargs)

    def delete_dss_nfs_export(self, **kwargs):
        return self.delete(**kwargs)