"""Define DSSWebAPI Data Export functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebProjectQuotas(DSSWebAPI):
    """Define the GET /projectquotas/ call in order to get all quotas."""

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['projectquotas', 'list']
        read_action = ['projectquotas', 'read']
        create_action = ['projectquotas', 'create']
        delete_action = ['projectquotas', 'delete']
        partial_update_action = ['projectquotas', 'partial_update']

    def list_project_quotas(self, **kwargs):
        """Return a list of project quotas."""
        return self.list(**kwargs)

    def read_project_quota(self, **kwargs):
        """Return a single project quota."""
        return self.read(**kwargs)

    def create_project_quota(self, **kwargs):
        """Create a new project quota."""
        return self.create(**kwargs)

    def update_project_quota(self, **kwargs):
        """Change a single project quota."""
        return self.partial_update(**kwargs)

    def delete_project_quota(self, **kwargs):
        """Delete a single project quota."""
        return self.delete(**kwargs)
