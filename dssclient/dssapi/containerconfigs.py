"""Define DSSWebAPI container config functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebContainerConfig(DSSWebAPI):
    """Define the GET /dsscontainerconfigs/ call in order to get all container configs."""

    class Meta:
        """Configure DSSWebAPI base class."""
        list_action = ['containerconfigs', 'list']
        read_action = ['containerconfigs', 'read']

    def list_dss_container_config(self, **kwargs):
        return self.list(**kwargs)

    def read_dss_container_config(self, **kwargs):
        return self.read(**kwargs)
