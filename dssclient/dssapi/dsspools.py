"""Define DSSWebAPI Data Pool functions."""
from dssclient.dssapi.common import DSSWebAPI


class DSSWebDSSPools(DSSWebAPI):
    """Define the GET /dsspools/ call in order to get all data pools."""

    class Meta:
        """Configure DSSWebAPI base class."""

        list_action = ['dsspools', 'list']
        read_action = ['dsspools', 'read']

    def list_pools(self, **kwargs):
        """Return a list of dsspools (list of hashes)."""
        return self.list(**kwargs)

    def read_pools(self, **kwargs):
        """Return a list of dsspools (list of hashes)."""
        return self.read(**kwargs)
