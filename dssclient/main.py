"""
Define the dsscli MAIN program.

For implementing the dsscli, we use cliff (Command Line Formulation Framework)
from the OpenStack project. For documentation regarding cliff, see
https://docs.openstack.org/cliff/latest/index.html
"""
import sys
import logging

import os
from cliff.app import App
from cliff.commandmanager import CommandManager

from dssclient.interactive import HardenedInteractiveApp


class DSSWebClientShell(App):
    """
    Define the main application class.

    Commands and their mapping to functions are defined in setup.py
    """

    def __init__(self, myname):
        """Define metadata like name and version."""
        if myname == 'rdsscli':
            iapp_fact = HardenedInteractiveApp
        else:
            iapp_fact = None
        super(DSSWebClientShell, self).__init__(
            description='DSSWeb Client',
            version='0.1',
            command_manager=CommandManager('dssclient'),
            deferred_help=True,
            interactive_app_factory=iapp_fact,
        )


def main(argv=sys.argv[1:]):
    """Ubuntu fix: Disable verbose output"""
    logging.getLogger('requests').setLevel(logging.ERROR)
    
    """Define the main function that calls into the cliff framework."""
    myname = os.path.basename(sys.argv[0])
    myapp = DSSWebClientShell(myname)
    return myapp.run(argv)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
