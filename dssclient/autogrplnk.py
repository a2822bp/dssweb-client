import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.common import get_item

from dssclient.dssapi.autogrplnk import DSSWebAutoGroupLink
from dssclient.dssapi.containers import DSSWebDSSContainers

from dssclient.auth import Token


class ListDSSAutoGrpLink(Lister):
    """List all available DSS auto group invitation links."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ListDSSAutoGrpLink, self).get_parser(prog_name)
        parser.add_argument('--projectname', nargs='?', help='Filter by name of project')
        parser.add_argument('--containername', nargs='?', help='Filter by name of container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli container list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI

        lnks = DSSWebAutoGroupLink(token=token)
        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        lnklist = []

        filter_fields = {}
        # Limit list to a parciular project, if specified by the user
        if parsed_args.projectname:
            filter_fields['dssdatacontainer__project_quota__data_project__name'] = parsed_args.projectname
        if parsed_args.containername:
            filter_fields['dssdatacontainer__name'] = parsed_args.containername

        lnklist = lnks.list_dss_autogrplnk(**filter_fields)

        # Prepare the output
        headers = ('ID', 'Container', 'Group', 'Group Origin', 'Access Mode', 'Quota GB', 'Quota Files')
        payload = ((
            p['id'],
            get_item(clist, p['dssdatacontainer'], 'url')['name'],
            p['group_name'],
            p['sim_service_name'],
            p['access_type_name'],
            p['quota_gb'],
            p['quota_files']

        ) for p in lnklist)
        return (headers, payload)


class ShowDSSAutoGrpLink(ShowOne):
    """Show details about a particular DSS auto group invitation link."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSAutoGrpLink, self).get_parser(prog_name)
        parser.add_argument('autogrplnkid', nargs=1, help='ID of the auto group invitation link to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        lnk = DSSWebAutoGroupLink(token=token)
        link = lnk.list_dss_autogrplnk(id=parsed_args.autogrplnkid[0])

        if len(link) != 1:
            raise RuntimeError(
                'Auto group link {} does not exists'.format(parsed_args.autogrplnkid[0]))
        link = link[0]

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        # Prepare the output
        rows = (
            'ID', 'Container', 'Group', 'Group Origin', 'Access Mode', 'Quota GB', 'Quota Files', 'Created',
            'Last Updated')

        data = (link['id'],
                get_item(clist, link['dssdatacontainer'], 'url')['name'],
                link['group_name'],
                link['sim_service_name'],
                link['access_type_name'],
                link['quota_gb'],
                link['quota_files'],
                link['created'],
                link['last_updated'],
                )
        return (rows, data)


class CreateDSSAutoGrpLink(ShowOne):
    """Create and show details about a new DSS auto group invitation link."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        accmodes = ['READ_WRITE', 'READ_ONLY']
        origins = ['DSS', 'TUMonline', 'LMU-VZ', 'LinuxCluster']  # , 'SuperMUC']

        parser = super(CreateDSSAutoGrpLink, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--containername', required=True,
                                 help='Name of the container to create the auto group link for')
        input_group.add_argument('--groupname', required=True, help='Name of the group to link as defined in LRZ IDM')
        input_group.add_argument('--grouporigin', required=True, choices=origins,
                                 help='Source system where the group has been defined')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--accessmode', choices=accmodes, required=False, default='READ_WRITE',
                                 help='Invitation access mode for all invitations, generated for this group for this container')
        input_group.add_argument('--maxgb', required=False,
                                 help='User capacity quota to define for all invitations, generated for this group for this container')
        input_group.add_argument('--maxfiles', required=False,
                                 help='User file quota to define for all invitations, generated for this group for this container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli autogrplnk show command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        container = containers.list_dss_containers(name=parsed_args.containername)

        if len(container) != 1:
            raise RuntimeError(
                'Container {} does not exist'.format(parsed_args.containername))
        container = container[0]
        body = {}
        body['dssdatacontainer'] = container['url']
        body['group_name'] = parsed_args.groupname
        body['sim_service'] = parsed_args.grouporigin
        if parsed_args.accessmode:
            body['access_type'] = parsed_args.accessmode
        if parsed_args.maxgb:
            body['quota_gb'] = parsed_args.maxgb
        if parsed_args.maxfiles:
            body['quota_files'] = parsed_args.maxfiles

        lnk = DSSWebAutoGroupLink(token=token)
        link = lnk.create_dss_autogrplnk(**body)
        rows = (
            'ID', 'Container', 'Group', 'Group Origin', 'Access Mode', 'Quota GB', 'Quota Files', 'Created',
            'Last Updated')
        data = (link['id'],
                container['name'],
                link['group_name'],
                link['sim_service_name'],
                link['access_type_name'],
                link['quota_gb'],
                link['quota_files'],
                link['created'],
                link['last_updated'],
                )
        return (rows, data)


class UpdateDSSAutoGrpLink(ShowOne):
    """Update and show details about a DSS auto group invitation link."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        accmodes = ['READ_WRITE', 'READ_ONLY']

        parser = super(UpdateDSSAutoGrpLink, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('autogrplnkid', nargs=1, help='ID of the auto group invitation link to update')
        input_group.add_argument('--accessmode', choices=accmodes, required=False, default='READ_WRITE',
                                 help='Invitation access mode for all invitations, generated for this group for this container')
        input_group.add_argument('--maxgb', required=False,
                                 help='User capacity quota to define for all invitations, generated for this group for this container')
        input_group.add_argument('--maxfiles', required=False,
                                 help='User file quota to define for all invitations, generated for this group for this container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        lnk = DSSWebAutoGroupLink(token=token)
        link = lnk.list_dss_autogrplnk(id=parsed_args.autogrplnkid[0])

        if len(link) != 1:
            raise RuntimeError(
                'Auto group invitation link {} does not exists'.format(parsed_args.autogrplnkid[0]))

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()
        body = {}
        if parsed_args.maxgb:
            body['quota_gb'] = parsed_args.maxgb
        if parsed_args.maxfiles:
            body['quota_files'] = parsed_args.maxfiles
        if parsed_args.accessmode:
            body['access_type'] = parsed_args.accessmode
        body['id'] = parsed_args.autogrplnkid[0]

        link = lnk.update_dss_autogrplnk(**body)
        rows = (
            'ID', 'Container', 'Group', 'Group Origin', 'Access Mode', 'Quota GB', 'Quota Files', 'Created',
            'Last Updated')
        data = (link['id'],
                get_item(clist, link['dssdatacontainer'], 'url')['name'],
                link['group_name'],
                link['sim_service_name'],
                link['access_type_name'],
                link['quota_gb'],
                link['quota_files'],
                link['created'],
                link['last_updated'],
                )

        return (rows, data)


class DeleteDSSAutoGrpLink(Command):
    """Delete a DSS auto group invitation link."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeleteDSSAutoGrpLink, self).get_parser(prog_name)
        parser.add_argument('autogrplnkid', nargs=1, help='ID of the auto group invitation link to update')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        lnk = DSSWebAutoGroupLink(token=token)
        link = lnk.list_dss_autogrplnk(id=parsed_args.autogrplnkid[0])

        if len(link) != 1:
            raise RuntimeError(
                'Auto group invitation link {} does not exists'.format(parsed_args.autogrplnkid[0]))
        link = link[0]
        lnk.delete_dss_autogrplnk(id=link['id'])
        self.app.stdout.write('Successfully deleted auto invitation group link {}!\n'.format(link['id']))
