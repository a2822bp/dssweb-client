import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.common import get_item

from dssclient.dssapi.containers import DSSWebDSSContainers

from dssclient.auth import Token
from dssclient.dssapi.globusinvitations import DSSWebDSSGlobusInvitations


class ListDSSGlobusInvitation(Lister):
    """List all available DSS Globus invitations."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ListDSSGlobusInvitation, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?')
        parser.add_argument('--containername', nargs='?')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli container list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI

        inv = DSSWebDSSGlobusInvitations(token=token)
        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        invlist = []

        filter_fields = {}
        # Limit list to a parciular project, if specified by the user
        if parsed_args.projectname:
            filter_fields['dssdatacontainer__project_quota__data_project__name'] = parsed_args.projectname
        if parsed_args.containername:
            filter_fields['dssdatacontainer__name'] = parsed_args.containername

        invlist = inv.list_globus_invitations(**filter_fields)

        # Prepare the output
        headers = ('ID', 'Container', 'User', 'Access Mode', 'Status')
        payload = ((
            p['id'],
            get_item(clist, p['dssdatacontainer'], 'url')['name'],
            p['user'],
            p['access_type_name'],
            p['userstatus'],

        ) for p in invlist)
        return (headers, payload)

class ShowDSSGlobusInvitationID(ShowOne):
    """Show details about a particular DSS Globus invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSGlobusInvitationID, self).get_parser(prog_name)
        parser.add_argument('containername', nargs=1)
        parser.add_argument('id', nargs=1)
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            name=parsed_args.containername[0],
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername[0]))
        cont = cont[0]

        inv = DSSWebDSSGlobusInvitations(token=token)
        invitation = inv.read_globus_invitation(id=parsed_args.id[0],
                                                containerid=cont['id'])

        if not invitation:
            raise RuntimeError(
                'Globus Invitation id {} to container {} does not exists'.format(parsed_args.id[0],
                                                                                 parsed_args.containername[0]))

        # Prepare the output
        rows = (
            'ID', 'Container', 'Subpath', 'Username', 'User ID', 'Name', 'Email', 'User Org', 'Access Mode', 'Status', 'Admin Role')

        data = (invitation['id'],
                cont['name'],
                invitation['path'],
                invitation['user'],
                invitation['uid'],
                invitation['fullname'],
                invitation['email'],
                invitation['userorg'],
                invitation['access_type_name'],
                invitation['userstatus'],
                invitation['role_type'],
                )
        return (rows, data)


class CreateDSSGlobusInvitation(ShowOne):
    """Create and show details about a new DSS Globus invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        accmodes = ['READ_WRITE', 'READ_ONLY']

        parser = super(CreateDSSGlobusInvitation, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--containername', required=True)
        input_group.add_argument('--email', required=True)
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--accessmode', choices=accmodes, required=False, default='READ_WRITE')
        input_group.add_argument('--path', required=False)
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            name=parsed_args.containername,
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername))
        cont = cont[0]
        body = {}
        body['dssdatacontainer'] = cont['url']
        body['email'] = parsed_args.email
        if parsed_args.accessmode:
            body['access_type'] = parsed_args.accessmode
        if parsed_args.path:
            body['path'] = parsed_args.path

        invitation = DSSWebDSSGlobusInvitations(token=token)
        inv = invitation.create_globus_invitation(**body)
        rows = (
            'ID', 'Container', 'Subpath', 'Username', 'User ID', 'Name', 'Email', 'User Org', 'Access Mode', 'Status',
            'Admin Role')

        data = (inv['id'],
                cont['name'],
                inv['path'],
                inv['user'],
                inv['uid'],
                inv['fullname'],
                inv['email'],
                inv['userorg'],
                inv['access_type_name'],
                inv['userstatus'],
                inv['role_type'],
                )
        return (rows, data)




class DeleteDSSGlobusInvitation(Command):
    """Delete a DSS Globus invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeleteDSSGlobusInvitation, self).get_parser(prog_name)
        parser.add_argument('containername', nargs=1)
        parser.add_argument('id', nargs=1)
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        cont = containers.list_dss_containers(
            name=parsed_args.containername[0],
        )
        if len(cont) != 1:
            raise RuntimeError(
                'Container {} does not exists'.format(parsed_args.containername[0]))
        cont = cont[0]

        inv = DSSWebDSSGlobusInvitations(token=token)
        invitation = inv.read_globus_invitation(id=parsed_args.id[0],
                                                containerid=cont['id'])

        if not invitation:
            raise RuntimeError(
                'Globus Invitation id {} to container {} does not exists'.format(parsed_args.id[0],
                                                                                parsed_args.containername[0]))

        inv.delete_globus_invitation(id=invitation['id'], containerid=cont['id'])
        self.app.stdout.write('Successfully initiated deletion of globus invitation {}!\n'.format(invitation['id']))



