import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.auth import Token
from dssclient.dssapi.projectquotas import DSSWebProjectQuotas
from dssclient.dssapi.dsspools import DSSWebDSSPools
from dssclient.dssapi.projects import DSSWebProjects
from dssclient.common import get_item


class ListProjectQuotas(Lister):
    """List all available project quotas."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add optional arguments poolname and projectname"""
        parser = super(ListProjectQuotas, self).get_parser(prog_name)
        parser.add_argument('--poolname', nargs='?', help='Filter by name of the pool')
        parser.add_argument('--projectname', nargs='?', help='Filter by name of the project')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota list command"""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all quotas from DSSWebAPI
        quotas = DSSWebProjectQuotas(token=token)
        pools = DSSWebDSSPools(token=token)

        filter_fields = {}

        if parsed_args.projectname:
            filter_fields['data_project__name'] = parsed_args.projectname
        if parsed_args.poolname:
            filter_fields['data_pool__name'] = parsed_args.poolname

        quotalist = quotas.list_project_quotas(**filter_fields)
        plist = pools.list_pools()

        # Prepare the output
        headers = ('ID',
                   'Project',
                   'Pool',
                   'Department',
                   'Subdepartment',
                   'Quota GB',
                   'Assinged GB',
                   'Used GB',
                   'Quota Files',
                   'Assigned Files',
                   'Used Files',
                   'Quota Cont',
                   'Assigned Cont',
                   )

        payload = ((
            p['id'],
            p['data_project'],
            get_item(plist, p['data_pool'], 'url')['name'],
            p['department'],
            p['subdepartment'],
            p['quota_gb'],
            p['used_gb'],
            p['phys_used_gb'],
            p['quota_files'],
            p['used_files'],
            p['phys_used_files'],
            p['quota_cont'],
            p['used_cont'],
        ) for p in quotalist)

        return (headers, payload)


class ShowProjectQuota(ShowOne):
    """Show details about a particular project quota."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments quota id."""
        parser = super(ShowProjectQuota, self).get_parser(prog_name)
        parser.add_argument('quotaid', nargs=1, help='ID of the project quota to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota show command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the quota and pool from DSSWeb by using filters
        quotas = DSSWebProjectQuotas(token=token)
        pools = DSSWebDSSPools(token=token)

        quota = quotas.list_project_quotas(id=parsed_args.quotaid[0])

        if len(quota) != 1:
            raise RuntimeError(
                'Quota {} does not exist'.format(parsed_args.quotaid[0]))
        quota = quota[0]

        plist = pools.list_pools()

        # Prepare the output
        rows = ('ID',
                'Project',
                'Pool',
                'Department',
                'Subdepartment',
                'Quota GB',
                'Assinged GB',
                'Used GB',
                'Quota Files',
                'Assigned Files',
                'Used Files',
                'Quota Cont',
                'Assigned Cont')

        data = (quota['id'],
                quota['data_project'],
                get_item(plist, quota['data_pool'], 'url')['name'],
                quota['department'],
                quota['subdepartment'],
                quota['quota_gb'],
                quota['used_gb'],
                quota['phys_used_gb'],
                quota['quota_files'],
                quota['used_files'],
                quota['phys_used_files'],
                quota['quota_cont'],
                quota['used_cont'])

        return (rows, data)


class CreateProjectQuota(ShowOne):
    """Create and show details about a new project quota."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments poolname, projectname, quota gb, files, container."""
        parser = super(CreateProjectQuota, self).get_parser(prog_name)

        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--poolname', required=True, help='Name of the pool assign the quota from')
        input_group.add_argument('--projectname', required=True, help='Name of the project to assign the quota to')
        input_group.add_argument('--quotagb', required=True, help='Capacity in GB to assign')
        input_group.add_argument('--quotafiles', required=True, help='Number of files to assign')
        input_group.add_argument('--quotacont', required=True, help='Number of containers to assign')

        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota create command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        pools = DSSWebDSSPools(token=token)
        pool = pools.list_pools(name=parsed_args.poolname)
        if len(pool) != 1:
            raise RuntimeError(
                'Project {} or pool {} do not exist'.format(parsed_args.projectname, parsed_args.poolname))
        pool = pool[0]

        body = {}
        body['data_pool'] = pool['url']
        body['data_project'] = parsed_args.projectname
        body['quota_gb'] = parsed_args.quotagb
        body['quota_files'] = parsed_args.quotafiles
        body['quota_cont'] = parsed_args.quotacont

        quotas = DSSWebProjectQuotas(token=token)
        quota = quotas.create_project_quota(**body)

        rows = ('ID',
                'Project',
                'Pool',
                'Quota GB',
                'Quota Files',
                'Quota Cont')

        data = (quota['id'],
                quota['data_project'],
                pool['name'],
                quota['quota_gb'],
                quota['quota_files'],
                quota['quota_cont'])

        return (rows, data)


class UpdateProjectQuota(ShowOne):
    """Update and show details about a project quota."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments quota id."""
        parser = super(UpdateProjectQuota, self).get_parser(prog_name)

        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('quotaid', nargs=1, help='ID of the quota to update')

        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--quotagb', required=False, help='Capacity in GB to assign')
        input_group.add_argument('--quotafiles', required=False, help='Number of files to assign')
        input_group.add_argument('--quotacont', required=False, help='Number of containers to assign')

        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota update command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        quotas = DSSWebProjectQuotas(token=token)
        quota = quotas.list_project_quotas(id=parsed_args.quotaid[0])

        if len(quota) != 1:
            raise RuntimeError(
                'Quota {} does not exist'.format(parsed_args.quotaid[0]))

        pools = DSSWebDSSPools(token=token)
        plist = pools.list_pools()

        body = {}

        if parsed_args.quotagb:
            body['quota_gb'] = parsed_args.quotagb

        if parsed_args.quotafiles:
            body['quota_files'] = parsed_args.quotafiles

        if parsed_args.quotacont:
            body['quota_cont'] = parsed_args.quotacont

        if body == {}:
            raise RuntimeError('No updated field specified')

        body['id'] = parsed_args.quotaid[0]

        quota = quotas.update_project_quota(**body)

        rows = ('ID',
                'Project',
                'Pool',
                'Quota GB',
                'Quota Files',
                'Quota Cont')

        data = (quota['id'],
                quota['data_project'],
                get_item(plist, quota['data_pool'], 'url')['name'],
                quota['quota_gb'],
                quota['quota_files'],
                quota['quota_cont'])

        return (rows, data)


class DeleteProjectQuota(Command):
    """Delete a particular project quota."""
    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments quota id."""
        parser = super(DeleteProjectQuota, self).get_parser(prog_name)

        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('quotaid', nargs=1, help='ID of the quota to revoke')

        return parser

    def take_action(self, parsed_args):
        """Define the dsscli quota delete command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        body = {}
        body['id'] = parsed_args.quotaid[0]

        quotas = DSSWebProjectQuotas(token=token)
        quota = quotas.delete_project_quota(**body)
        self.app.stdout.write('Successfully deleted storage grant {}\n'.format(body['id']))
