import logging

from cliff.command import Command
from cliff.lister import Lister
from cliff.show import ShowOne

from dssclient.common import get_item

from dssclient.dssapi.containers import DSSWebDSSContainers

from dssclient.auth import Token
from dssclient.dssapi.invitations import DSSWebDSSInvitations


class ListDSSInvitation(Lister):
    """List all available DSS invitations."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(ListDSSInvitation, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?', help='Filter by name of the project')
        parser.add_argument('--username', nargs='?', help='Filter by name of the invited user')
        parser.add_argument('--containername', nargs='?', help='Filter by name of the container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli container list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI

        inv = DSSWebDSSInvitations(token=token)
        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        invlist = []

        filter_fields = {}
        # Limit list to a parciular project, if specified by the user
        if parsed_args.projectname:
            filter_fields['dssdatacontainer__project_quota__data_project__name'] = parsed_args.projectname
        if parsed_args.username:
            filter_fields['username'] = parsed_args.username
        if parsed_args.containername:
            filter_fields['dssdatacontainer__name'] = parsed_args.containername

        invlist = inv.list_dss_invitations(**filter_fields)

        # Prepare the output
        headers = ('ID', 'Container', 'User', 'Access Mode', 'Is Manager', 'Status')
        payload = ((
            p['id'],
            get_item(clist, p['dssdatacontainer'], 'url')['name'],
            p['username'],
            p['access_type_name'],
            p['is_container_manager'],
            p['status_name'],

        ) for p in invlist)
        return (headers, payload)


class ShowDSSInvitationID(ShowOne):
    """Show details about a particular DSS invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSInvitationID, self).get_parser(prog_name)
        parser.add_argument('invitationid', nargs=1, help='ID of the invitation to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        inv = DSSWebDSSInvitations(token=token)
        invitation = inv.list_dss_invitations(id=parsed_args.invitationid[0])

        if len(invitation) != 1:
            raise RuntimeError(
                'Invitation {} does not exists'.format(parsed_args.invitationid[0]))
        invitation = invitation[0]

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        # Prepare the output
        rows = (
            'ID', 'Container', 'User', 'Firstname', 'Lastname', 'Quota GB', 'Used GB', 'Used GB Percentage',
            'Quota Files', 'Used Files', 'Used Files Percentage', 'Access Mode', 'Is Manager', 'Status', 'Active Task',
            'Expires At',
            'Inviter', 'Created', 'Last Updated')

        data = (invitation['id'],
                get_item(clist, invitation['dssdatacontainer'], 'url')['name'],
                invitation['username'],
                invitation['firstname'],
                invitation['lastname'],
                invitation['quota_gb'],
                invitation['used_gb'],
                invitation['used_gb_pct'],
                invitation['quota_files'],
                invitation['used_files'],
                invitation['used_files_pct'],
                invitation['access_type_name'],
                invitation['is_container_manager'],
                invitation['status_name'],
                invitation['active_task_id'],
                invitation['expires_at'],
                invitation['inviter'],
                invitation['created'],
                invitation['last_updated'],
                )
        return (rows, data)


class ShowDSSInvitation(ShowOne):
    """Show details about a particular DSS invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        parser = super(ShowDSSInvitation, self).get_parser(prog_name)
        parser.add_argument('containername', nargs=1, help='Name of the container of the invitation to show')
        parser.add_argument('username', nargs=1, help='Name of the user of the invitation to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        inv = DSSWebDSSInvitations(token=token)
        invitation = inv.list_dss_invitations(username=parsed_args.username[0],
                                              dssdatacontainer__name=parsed_args.containername[0])

        if len(invitation) != 1:
            raise RuntimeError(
                'Invitation for user {} to container {} does not exists'.format(parsed_args.username[0],
                                                                                parsed_args.containername[0]))
        invitation = invitation[0]

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()

        # Prepare the output
        rows = (
            'ID', 'Container', 'User', 'Firstname', 'Lastname', 'Quota GB', 'Used GB', 'Used GB Percentage',
            'Quota Files', 'Used Files', 'Used Files Percentage', 'Access Mode', 'Is Manager', 'Status', 'Active Task',
            'Expires At',
            'Inviter', 'Created', 'Last Updated')

        data = (invitation['id'],
                get_item(clist, invitation['dssdatacontainer'], 'url')['name'],
                invitation['username'],
                invitation['firstname'],
                invitation['lastname'],
                invitation['quota_gb'],
                invitation['used_gb'],
                invitation['used_gb_pct'],
                invitation['quota_files'],
                invitation['used_files'],
                invitation['used_files_pct'],
                invitation['access_type_name'],
                invitation['is_container_manager'],
                invitation['status_name'],
                invitation['active_task_id'],
                invitation['expires_at'],
                invitation['inviter'],
                invitation['created'],
                invitation['last_updated'],
                )
        return (rows, data)


class DSSInvitationCreate(ShowOne):
    """Create and show details about a new DSS invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        accmodes = ['READ_WRITE', 'READ_ONLY']
        mgrmodes = ['YES', 'NO']

        parser = super(DSSInvitationCreate, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('--containername', required=True,
                                 help='Name of the container to create the invitation for')
        input_group.add_argument('--username', required=True, help='Name of the user to create the invitation for')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--accessmode', choices=accmodes, required=False, default='READ_WRITE',
                                 help='Access mode of the invited user to the container')
        input_group.add_argument('--maxgb', required=False,
                                 help='Maximum usable capacity in GB the user can consume in the container')
        input_group.add_argument('--maxfiles', required=False,
                                 help='Maximum number of files the user can create in the container')
        input_group.add_argument('--ismanager', choices=mgrmodes, required=False, default='NO',
                                 help='Allow this user to manage properties like invitations, NFS exports, etc. for this container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        containers = DSSWebDSSContainers(token=token)
        container = containers.list_dss_containers(name=parsed_args.containername)

        if len(container) != 1:
            raise RuntimeError(
                'Container {} does not exist'.format(parsed_args.containername))
        container = container[0]
        body = {}
        body['dssdatacontainer'] = container['url']
        body['username'] = parsed_args.username
        if parsed_args.accessmode:
            body['access_type'] = parsed_args.accessmode
        if parsed_args.maxgb:
            body['quota_gb'] = parsed_args.maxgb
        if parsed_args.maxfiles:
            body['quota_files'] = parsed_args.maxfiles
        if parsed_args.ismanager:
            body['is_container_manager'] = parsed_args.ismanager
        invitation = DSSWebDSSInvitations(token=token)
        inv = invitation.create_dss_invitation(**body)
        rows = (
            'ID', 'Container', 'User', 'Quota GB', 'Quota Files', 'Access Mode', 'Is Manager', 'Status', 'Active Task',
            'Expires At',
            'Inviter')
        data = (inv['id'],
                container['name'],
                inv['username'],
                inv['quota_gb'],
                inv['quota_files'],
                inv['access_type_name'],
                inv['is_container_manager'],
                inv['status_name'],
                inv['active_task_id'],
                inv['expires_at'],
                inv['inviter']
                )

        return (rows, data)


class UpdateDSSInvitation(ShowOne):
    """Update and show details about a DSS invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments container name."""
        accmodes = ['READ_WRITE', 'READ_ONLY']
        mgrmodes = ['YES', 'NO']

        parser = super(UpdateDSSInvitation, self).get_parser(prog_name)
        input_group = parser.add_argument_group(title='input data', description='mandatory input options', )
        input_group.add_argument('invitationid', nargs=1, help='ID of the invitation to update')
        input_group = parser.add_argument_group(title='input data', description='optional input options', )
        input_group.add_argument('--accessmode', choices=accmodes, required=False,
                                 help='Access mode of the invited user to the container')
        input_group.add_argument('--maxgb', required=False,
                                 help='Maximum usable capacity in GB the user can consume in the container')
        input_group.add_argument('--maxfiles', required=False,
                                 help='Maximum number of files the user can create in the container')
        input_group.add_argument('--prolong', required=False, action='store_true',
                                 help='Prolong expire date of a still pending invitation')
        input_group.add_argument('--resend', required=False, action='store_true', help='Resend invitation mail')
        input_group.add_argument('--ismanager', choices=mgrmodes, required=False,
                                 help='Allow this user to manage properties like invitations, NFS exports, etc. for this container')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        invitation = DSSWebDSSInvitations(token=token)
        inv = invitation.list_dss_invitations(id=parsed_args.invitationid[0])

        if len(inv) != 1:
            raise RuntimeError(
                'Invitation {} does not exists'.format(parsed_args.invitationid[0]))

        containers = DSSWebDSSContainers(token=token)
        clist = containers.list_dss_containers()
        body = {}
        if parsed_args.accessmode:
            body['access_type'] = parsed_args.accessmode
        if parsed_args.maxgb:
            body['quota_gb'] = parsed_args.maxgb
        if parsed_args.maxgb:
            body['quota_files'] = parsed_args.maxfiles
        if parsed_args.prolong:
            body['prolong_invitation'] = parsed_args.prolong
        if parsed_args.resend:
            body['resend_invitation'] = parsed_args.resend
        if parsed_args.ismanager:
            body['is_container_manager'] = parsed_args.ismanager
        if body == {}:
            raise RuntimeError('No updated field specified')
        body['id'] = parsed_args.invitationid[0]

        inv = invitation.update_dss_invitation(**body)
        rows = (
            'ID', 'Container', 'User', 'Quota GB', 'Quota Files', 'Access Mode', 'Is Manager', 'Status', 'Active Task',
            'Expires At',
            'Inviter')
        data = (inv['id'],
                get_item(clist, inv['dssdatacontainer'], 'url')['name'],
                inv['username'],
                inv['quota_gb'],
                inv['quota_files'],
                inv['access_type_name'],
                inv['is_container_manager'],
                inv['status_name'],
                inv['active_task_id'],
                inv['expires_at'],
                inv['inviter']
                )

        return (rows, data)


class DeleteDSSInvitation(Command):
    """Delete a DSS invitation."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(DeleteDSSInvitation, self).get_parser(prog_name)
        parser.add_argument('containername', nargs=1, help='Name of the container to delete the invitation in')
        parser.add_argument('username', nargs=1, help='Name of the user to delete the invitation for')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli dsscontainer show containername command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        inv = DSSWebDSSInvitations(token=token)
        invitation = inv.list_dss_invitations(username=parsed_args.username[0],
                                              dssdatacontainer__name=parsed_args.containername[0])

        if len(invitation) != 1:
            raise RuntimeError(
                'Invitation for user {} to container {} does not exists'.format(parsed_args.username[0],
                                                                                parsed_args.containername[0]))
        invitation = invitation[0]
        inv.delete_dss_invitation(id=invitation['id'])
        self.app.stdout.write('Successfully initiated deletion of invitation {}!\n'.format(invitation['id']))


class GeneratePasswd(Command):
    """Generate passwd file from invitations."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        parser = super(GeneratePasswd, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?', help='Filter by name of project')
        parser.add_argument('--containername', nargs='?', help='Filter by name of container')
        return parser

    def take_action(self, parsed_args):
        # sample line: di52dob:x:1004:1006::/home/di52dob:
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        inv = DSSWebDSSInvitations(token=token)

        invlist = []

        filter_fields = {}
        # Limit list to a particular project, if specified by the user
        if parsed_args.projectname:
            filter_fields['dssdatacontainer__project_quota__data_project__name'] = parsed_args.projectname
        if parsed_args.containername:
            filter_fields['dssdatacontainer__name'] = parsed_args.containername

        invlist = inv.list_dss_invitations(**filter_fields)

        users = {}
        for invitation in invlist:
            if invitation['status'] == 3:  # 3 == (INV_STAT_USER_ACCEPTED, 'USER ACCEPTED'), wegen [DSM-104]
                users[invitation['uid']] = '{}:x:{}:2222::/home/{}:\n'.format(invitation['username'], invitation['uid'],
                                                                              invitation['username'])

        self.app.stdout.write(''.join("{!s}".format(val) for (key, val) in users.items()))
