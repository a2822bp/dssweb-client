"""Define the dsscli commands that operate on data pools."""

import logging
from cliff.lister import Lister
from cliff.show import ShowOne
from dssclient.auth import Token
from dssclient.dssapi.pools import DSSWebPools
from dssclient.dssapi.projects import DSSWebProjects
from dssclient.common import get_item


class ListPools(Lister):
    """List all available data pools."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """
        Add optional argument projectname.

        If projectname is specified, we only list pools for the particular
        project.
        """
        parser = super(ListPools, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs='?', help='Filter by name of project')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli pool list command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get all projects and pools from DSSWebAPI
        pools = DSSWebPools(token=token)
        polist = []

        projects = DSSWebProjects(token=token)
        prlist = projects.list_projects()

        # Limit list to a parciular project, if specified by the user
        if parsed_args.projectname:
            polist = pools.list_pools(
                data_project__name=parsed_args.projectname
            )
        else:
            polist = pools.list_pools()

        # Prepare the output
        headers = ('ID', 'Project', 'Name')
        payload = ((
            p['id'],
            get_item(prlist, p['data_project'], 'url')['name'],
            p['data_pool']['name'],
        ) for p in polist)
        return (headers, payload)


class ShowPool(ShowOne):
    """Show details about a particular data pool."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments projectname and poolname."""
        parser = super(ShowPool, self).get_parser(prog_name)
        parser.add_argument('projectname', nargs=1, help='Name of the project the pool was assigned to')
        parser.add_argument('poolname', nargs=1, help='Name of the pool to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli project show projectname poolname command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        pools = DSSWebPools(token=token)
        pool = pools.list_pools(
            data_pool__name=parsed_args.poolname[0],
            data_project__name=parsed_args.projectname[0],
        )
        if len(pool) != 1:
            raise RuntimeError(
                'Pool {} does not exists for project {}'.format(parsed_args.poolname[0], parsed_args.projectname[0]))
        pool = pool[0]

        # Prepare the output
        rows = ('ID', 'Name', 'Project', 'Description', 'Hints', 'Owner',
                'Quota GB', 'Free GB', 'Used GB', 'Used GB Percentage', 'Quota Files', 'Free Files', 'Used Files',
                'Used Files Percentage', 'Quota Containers', 'Free Containers', 'Used Containers',
                'Used Containers Percentage', 'Is Lightweight Pool')
        data = (pool['id'],
                pool['data_pool']['name'],
                parsed_args.projectname[0],
                pool['data_pool']['description'],
                pool['data_pool']['hints'],
                pool['data_pool']['owner'],
                pool['quota_gb'],
                pool['free_gb'],
                pool['used_gb'],
                pool['used_gb_pct'],
                pool['quota_files'],
                pool['free_files'],
                pool['used_files'],
                pool['used_files_pct'],
                pool['quota_cont'],
                pool['free_cont'],
                pool['used_cont'],
                pool['used_cont_pct'],
                pool['data_pool']['lightweight_pool'],)
        return (rows, data)


class ShowPoolID(ShowOne):
    """Show details about a particular data pool."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add mandatory arguments projectname and poolname."""
        parser = super(ShowPoolID, self).get_parser(prog_name)
        parser.add_argument('poolid', nargs=1, help='ID of the pool to show')
        return parser

    def take_action(self, parsed_args):
        """Define the dsscli project show projectname poolname command."""
        jwt = Token()
        jwt.refresh()
        (user, token, refresh) = jwt.read()

        # Get the pool from DSSWeb by using filters
        pools = DSSWebPools(token=token)
        pool = pools.read_pools(
            id=parsed_args.poolid[0],
        )
        projects = DSSWebProjects(token=token)
        plist = projects.list_projects()
        # Prepare the output
        rows = ('ID', 'Name', 'Project', 'Description', 'Hints', 'Owner',
                'Quota GB', 'Free GB', 'Used GB', 'Used GB Percentage', 'Quota Files', 'Free Files', 'Used Files',
                'Used Files Percentage', 'Quota Containers', 'Free Containers', 'Used Containers',
                'Used Containers Percentage', 'Is Lightweight Pool')
        data = (pool['id'],
                pool['data_pool']['name'],
                get_item(plist, pool['data_project'], 'url')['name'],
                pool['data_pool']['description'],
                pool['data_pool']['hints'],
                pool['data_pool']['owner'],
                pool['quota_gb'],
                pool['free_gb'],
                pool['used_gb'],
                pool['used_gb_pct'],
                pool['quota_files'],
                pool['free_files'],
                pool['used_files'],
                pool['used_files_pct'],
                pool['quota_cont'],
                pool['free_cont'],
                pool['used_cont'],
                pool['used_cont_pct'],
                pool['data_pool']['lightweight_pool'],)
        return (rows, data)
