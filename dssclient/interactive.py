import shlex

import itertools

import os
from os.path import expanduser

import cmd2
from cliff.interactive import InteractiveApp

class HardenedInteractiveApp(InteractiveApp):

    def default_fix(self, line):
        # This function is broken in the current implementation of cliff. Therefore, we 'fix' it here.
        line_parts = shlex.split(line)
        self.parent_app.run_subcommand(line_parts)

    def do_help(self, arg):
        # this function is broken in the current implementation of cliff. Therefore, we 'fix' it here.
        if arg:
            # Check if the arg is a builtin command or something
            # coming from the command manager
            arg_parts = shlex.split(arg)
            method_name = '_'.join(
                itertools.chain(
                    ['do'],
                    itertools.takewhile(lambda x: not x.startswith('-'),
                                        arg_parts)
                )
            )
            # Have the command manager version of the help
            # command produce the help text since cmd and
            # cmd2 do not provide help for "help"
            if hasattr(self, method_name):
                return cmd2.Cmd.do_help(self, arg)
            # Dispatch to the underlying help command,
            # which knows how to provide help for extension
            # commands.
            self.default_fix('help ' + arg)
        else:
            cmd2.Cmd.do_help(self, arg)
            cmd_names = sorted([n for n, v in self.command_manager])
            self.print_topics(self.app_cmd_header, cmd_names, 15, 80)
        return

    def do_edit(self, args):
        """Shell operations are not supported by dsscli remove service for security reasons."""
        if not self.editor:
            raise EnvironmentError("Please use 'set editor' to specify your text editing program of choice.")
        homedir = expanduser("~")
        filename = '{}/.ssh/authorized_keys'.format(homedir)
        os.system('"{}" "{}"'.format(self.editor, filename))
        return

    def do_load(self, args):
        """Shell operations are not supported by dsscli remove service for security reasons."""
        print('Shell operations are not supported by dsscli remove service for security reasons.')
        return

    def do_py(self, args):
        """Shell operations are not supported by dsscli remove service for security reasons."""
        print('Shell operations are not supported by dsscli remove service for security reasons.')
        return

    def do_pyscript(self, args):
        """Shell operations are not supported by dsscli remove service for security reasons."""
        print('Shell operations are not supported by dsscli remove service for security reasons.')
        return

    def do_save(self, args):
        """Shell operations are not supported by dsscli remove service for security reasons."""
        print('Shell operations are not supported by dsscli remove service for security reasons.')
        return

    def do_shell(self, args):
        """Shell operations are not supported by dsscli remove service for security reasons."""
        print('Shell operations are not supported by dsscli remove service for security reasons.')
        return