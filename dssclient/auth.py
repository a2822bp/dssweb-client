"""
Define commands and tools for handling authentication tokens.

dsscli uses JSON Web Tokens for authentication against the DSSWeb API.
This module contains the necessary tools for handling these tokens.
"""
import logging
from cliff.command import Command
from dssclient.dssapi.auth import (
    DSSWebTokenAuth, DSSWebTokenRefresh, DSSWebTokenVerify
)
import getpass
import os


class Token(object):
    """
    Define token handling.

    dsscli stores auth tokens in $HOME/.dsswebclient/token
    This class provides tools to handle them easily.
    """

    log = logging.getLogger(__name__)

    def __init__(self):
        """Setup paths."""
        self._home = os.path.expanduser('~')
        self._tokdir = self._home + '/.dsswebclient/'
        self._tokfile = self._tokdir + 'token'

    def read(self):
        """
        Read the auth token from the file system.

        Returns a list containing [username,token]
        """
        self.log.debug('Reading token from {}'.format(self._tokfile))
        try:
            fh = open(self._tokfile, 'r')
            raw_token = fh.readline()
        except:
            raise RuntimeError("Couldn't read token file {}. "
                               "Use login to get a token.".format(
                                    self._tokfile)
                               )
        self.log.debug("Read token {}".format(raw_token))
        if 'DSSTOKEN:' in raw_token:
            return raw_token.split(':', 3)[1:]
        else:
            raise RuntimeError(
                'No authentication token. Use login to get one.'
            )

    def store(self, user, access_token, refresh_token):
        """
        Write an auth token to the file system.

        Takes username and token as arguments and write the token file to
        $HOME/.dsswebclient/token
        """
        self.log.debug('Checking if token directory {} exists'.format(
            self._tokdir)
        )
        if not os.path.exists(self._tokdir):
            self.log.debug('Token directory does not exist')
            self.log.debug('Creating token directory with 0600')
            os.makedirs(self._tokdir)
        else:
            self.log.debug('Token directory does exist')

        self.log.debug('Making sure it is 0700')
        os.chmod(self._tokdir, 0o700)

        self.log.debug("Writing token to {}".format(self._tokfile))
        open(self._tokfile, 'w').close()
        os.chmod(self._tokfile, 0o600)
        fh = open(self._tokfile, 'a')
        fh.write("DSSTOKEN:{}:{}:{}".format(user, access_token, refresh_token))
        fh.close()

    def refresh(self):
        """
        Refresh the current token.

        Reads the token from the file system and tries to get a fresh one from
        the DSSWeb API. If sucessfull, new token is written back to file system

        Raises an RuntimeError if token couldn't be refrehsed.
        """
        (user, access_token, refresh_token) = self.read()
        self.log.debug('Trying to refresh token {}'.format(refresh_token))
        tverify = DSSWebTokenVerify()
        auth = DSSWebTokenRefresh()
        self.log.debug('Trying to verify current token')
        if tverify.verify_token(refresh_token):
            self.log.debug('Token is still valid')
            self.log.debug('Trying to refresh current token')
            try:
                token = auth.refresh_token(refresh_token)
            except:
                raise RuntimeError("Couldn't refresh token. Maybe max "
                                   "lifetime expired. Use login to get a "
                                   "new one."
                                   )
            self.log.debug('Successfully refresed token {}'.format(token))
            self.store(user, token['access'], refresh_token)
        else:
            raise RuntimeError(
                'Current authentication token invalid. '
                'Use login to get a new one.'
            )


class Login(Command):
    """Request username and password and obtain a token from DSSWeb API."""

    log = logging.getLogger(__name__)

    def get_parser(self, prog_name):
        """Add optional arguments container name."""

        parser = super(Login, self).get_parser(prog_name)
        parser.add_argument('--username', required=False)
        parser.add_argument('--password', required=False)
        return parser

    def take_action(self, parsed_args):
        """Get username and pass and obtain an JWT from DSSWeb."""
        self.log.debug('Creating DSSWeb Authentication object')
        auth = DSSWebTokenAuth()

        if not parsed_args.username:
            self.log.debug('Obtaining username and password from user')
            u = input("Username: ")
        else:
            u = parsed_args.username
        if not parsed_args.password:
            p = getpass.getpass(prompt='Password: ')
        else:
            p = parsed_args.password

        self.log.debug('Trying to get authentication token from DSSWeb')
        token = auth.get_token(u, p)
        self.log.debug('Got token {}'.format(token))

        jwt = Token()
        jwt.store(u, token['access'], token['refresh'])

        self.log.debug("Successfully logged in {}".format(u))
        self.app.stdout.write('Logged in.\n')
